package br.edu.ufcg.powerquiz.db;

import android.content.Context;
import android.content.SharedPreferences;

public class DBFlash {
	
	public static DBFlash instance;
	private final String LOCALFILE = "dbfile";
	public static String USERCONF = "userConf";
	public static String USERREMEMBER = "userRemember";
	public static String QUIZRESULT = "quizResult";
	
	public static String FACEBOOKTOKEN = "access_token";
	public static String FACEBOOKEXPIRES = "access_expires";
	
	public static String FACEBOOKAPPID = "499047190179445";
	
	public static String TWITTER_CONSUMER_KEY = "H2ZI2C31B4kGCJfPFoBA";
	public  static String TWITTER_CONSUMER_SECRET = "OATLHdTePtBS1ntb7z02TIiCye9S9mNLD43abKovr8";
 
    // Preference Constants
	public static String PREFERENCE_NAME = "twitter_oauth";
	public static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
	public static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
	public static final String PREF_KEY_TWITTER_LOGIN = "isTwitterLogedIn";
 
	public static final String TWITTER_CALLBACK_URL = "oauth://powerquiz";
 
    // Twitter oauth urls
	public static final String URL_TWITTER_AUTH = "auth_url";
	public static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
	public static final String URL_TWITTER_OAUTH_TOKEN = "oauth_token";
	
	
	private DBFlash() {}
	
	public static DBFlash getInstace() {
		if(instance == null) {
			instance = new DBFlash();
		}
		return instance;
	}
	
	public void saveConfigDB(String attr, Context ctx, String file) {
		SharedPreferences settings = ctx.getApplicationContext().getSharedPreferences(LOCALFILE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(file, attr);

		editor.commit();
	}
	
	public String retriveConfigDB(Context ctx, String file) {
		SharedPreferences settings = ctx.getApplicationContext().getSharedPreferences(LOCALFILE, 0);
		return settings.getString(file, "");
	}
	
}
