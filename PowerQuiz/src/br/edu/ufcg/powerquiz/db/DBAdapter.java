package br.edu.ufcg.powerquiz.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBAdapter {

	private static final String TAB_MODULE = "tab_module";
	private static final String TAB_QUIZ = "tab_quiz";
	private static final String TAB_ALTERNATIVE = "tab_alternative";
	private static final String TAB_VERSION = "tab_version";
	private static final String TAB_STATISTIC = "tab_statistic";
	private static final String TAB_CHALLENGE = "tab_challenge";

	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;
	private Context mCtx;
	public static String user;

	private static final String DB_NAME = "iquiz_DB";
	private static final int DATABASE_VERSION = 3;

	private final static String CREATEMODULE = "CREATE TABLE " + TAB_MODULE
			+ "(mod_id INTEGER(8) NOT NULL PRIMARY KEY, "
			+ "name varchar(255) NOT NULL," + "description text NOT NULL,"
			+ "author_id INTEGER(8) NOT NULL,"
			+ "category_id INTEGER(8) NOT NULL," + "price float NOT NULL)";

	private final static String CREATEQUIZ = "CREATE TABLE "
			+ TAB_QUIZ
			+ "( quiz_id INTEGER(8) NOT NULL PRIMARY KEY,"
			+ "mod_id INTEGER(8) NOT NULL REFERENCES tab_module(mod_id) ON DELETE CASCADE,"
			+ "question text NOT NULL)";

	private final static String CREATEQUIZALTERNATIVE = "CREATE TABLE "
			+ TAB_ALTERNATIVE
			+ "(alternative_id INTEGER(8) NOT NULL PRIMARY KEY,"
			+ "quiz_id INTEGER(8) NOT NULL REFERENCES tab_quiz(quiz_id) ON DELETE CASCADE,"
			+ "alternative text NOT NULL," + "correct INTEGER(2) NOT NULL)";

	private final static String CREATEVERSION = "CREATE TABLE "
			+ TAB_VERSION
			+ "(mod_id INTEGER(8) NOT NULL PRIMARY KEY REFERENCES tab_module(mod_id) ON DELETE CASCADE,"
			+ " version INTEGER(8) NOT NULL)";
	
	private final static String CREATESTATISTIC = "CREATE TABLE "
			+ TAB_STATISTIC
			+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ " mod_id INTEGER(8) NOT NULL REFERENCES tab_module(mod_id) ON DELETE CASCADE, "
			+ " hits INTEGER(2) NOT NULL, "
			+ " total INTEGER(2) NOT NULL)";
	
	private final static String CREATECHALLENGE = "CREATE TABLE "
			+ TAB_CHALLENGE
			+ "(challenging INTEGER(8) NOT NULL,"
			+ " challenged INTEGER(8) NOT NULL,"
			+ " mod_id INTEGER(8) NOT NULL,"
			+ " executed INTEGER(8) NOT NULL,"
			+ " PRIMARY KEY (challenging, challenged, mod_id))";

	public DBAdapter(Context ctx, String user) {
		this.mCtx = ctx;
		DBAdapter.user = user;
	}

	public DBAdapter open() throws SQLException {
		mDbHelper = new DatabaseHelper(mCtx);
		mDb = mDbHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		mDbHelper.close();
		mDb.close();
	}

	public void insertIntoModule(int mod_id, String name, String description,
			int author_id, int category_id, float price, int version) {
		ContentValues values = new ContentValues();
		values.put("mod_id", mod_id);
		values.put("name", name);
		values.put("description", description);
		values.put("author_id", author_id);
		values.put("category_id", category_id);
		values.put("price", price);
		mDb.insert(TAB_MODULE, null, values);
		values.clear();
		this.insertIntoVersion(mod_id, version);
	}
	
	public void insertIntoVersion(int mod_id, int version) {
		ContentValues values = new ContentValues();
		values.put("mod_id", mod_id);
		values.put("version", version);
		mDb.insert(TAB_VERSION, null, values);
		values.clear();
		
	}

	public int numberOfModulesByID(int mod_id) {
		Cursor mCount = mDb.rawQuery(
				"SELECT COUNT(*)  FROM tab_module WHERE mod_id = " + mod_id,
				null);
		mCount.moveToFirst();
		int count = mCount.getInt(0);
		mCount.close();
		return count;
	}

	public Cursor retriveAllModuleIdAndVersion() throws SQLException {
		return mDb.query(true, TAB_VERSION,
				new String[] { "mod_id", "version" }, null, null, null, null,
				null, null);
	}

	public Cursor retriveModuleById(int modId) throws SQLException {

		return mDb.query(true, TAB_MODULE, new String[] { "mod_id", "name",
				"description", "author_id", "category_id", "price" },
				"mod_id=?", new String[] { String.valueOf(modId) }, null, null,
				null, null);

	}
	
	public Cursor retriveAllModule() throws SQLException {

		return mDb.query(true, TAB_MODULE, new String[] { "mod_id", "name",
				"description", "author_id", "category_id", "price" }, null,
				null, null, null, null, null);

	}

	public void deleteModule(int mod_id) {

		mDb.delete(TAB_MODULE, "mod_id=" + mod_id, null);
	}

	public void insertIntoQuiz(int quiz_id, int mod_id, String question) {
		ContentValues values = new ContentValues();
		values.put("quiz_id", quiz_id);
		values.put("mod_id", mod_id);
		values.put("question", question);
		mDb.insert(TAB_QUIZ, null, values);
		values.clear();
	}

	public Cursor listQuizById(int quiz_id) {
		return mDb.query(true, TAB_QUIZ, new String[] { "quiz_id", "mod_id",
				"question" }, "quiz_id=?",
				new String[] { String.valueOf(quiz_id) }, null, null, null,
				null);

	}

	public Cursor listQuizByModule(int mod_id, int limit) {
		return mDb
				.query(true, TAB_QUIZ, new String[] { "quiz_id", "mod_id",
						"question" }, "mod_id=? ORDER BY RANDOM() LIMIT " + limit,
						new String[] { String.valueOf(mod_id) }, null, null,
						null, null);
	}
	
	public void insertIntoAlternative(int alternative_id, int quiz_id,
		String alternative, int correct) {
		ContentValues values = new ContentValues();
		values.put("alternative_id", alternative_id);
		values.put("quiz_id", quiz_id);
		values.put("alternative", alternative);
		values.put("correct", correct);
		mDb.insert(TAB_ALTERNATIVE, null, values);
		values.clear();
	}

	public Cursor listQuizAlternativeById(int alternative_id) {
		return mDb.query(true, TAB_ALTERNATIVE, new String[] {
				"alternative_id", "quiz_id", "alternative", "correct" },
				"alternative_id=?",
				new String[] { String.valueOf(alternative_id) }, null, null,
				null, null);
	}

	public Cursor listAlternativesByQuiz(int quiz_id) {
		return mDb.query(true, TAB_ALTERNATIVE, new String[] {
				"alternative_id", "quiz_id", "alternative", "correct" },
				"quiz_id=?", new String[] { String.valueOf(quiz_id) }, null,
				null, null, null);
	}
	
	public void insertIntoStatistic(int mod_id, int hits, int total) {
		ContentValues values = new ContentValues();
		values.put("mod_id", mod_id);
		values.put("hits", hits);
		values.put("total", total);
		mDb.insert(TAB_STATISTIC, null, values);
		values.clear();
	}
	
	public Cursor listStatisticByModule(int mod_id) {
		//return mDb.query(true, TAB_STATISTIC, new String[] {
		//		"mod_id", "hits", "total" },
		//		"mod_id=?", new String[] { String.valueOf(mod_id) }, null,
		//		null, null, null);
		return mDb.rawQuery("Select mod_id, hits, total FROM tab_statistic WHERE mod_id = " + mod_id, null);
	}
	
	public void insertIntoChallenge(int user1, int user2, int mod_id) {
		ContentValues values = new ContentValues();
		values.put("challenging", user1);
		values.put("challenged", user2);
		values.put("mod_id", mod_id);
		mDb.insert(TAB_CHALLENGE, null, values);
		values.clear();
	}
	
	public Cursor listChallengeHistorical() {
		return mDb.query(true, TAB_CHALLENGE, new String[] {
				"challenging", "challenged", "mod_id" },
				null, null, null,
				null, null, null);
	}
	

	private static class DatabaseHelper extends SQLiteOpenHelper {

		public DatabaseHelper(Context context) {
			super(context, user+DB_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onOpen(SQLiteDatabase db) {
			super.onOpen(db);
			if (!db.isReadOnly()) {
				db.execSQL("PRAGMA foreign_keys=ON;");
			}
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATEMODULE);
			db.execSQL(CREATEQUIZ);
			db.execSQL(CREATEQUIZALTERNATIVE);
			db.execSQL(CREATEVERSION);
			db.execSQL(CREATESTATISTIC);
			db.execSQL(CREATECHALLENGE);

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + TAB_MODULE);
			db.execSQL("DROP TABLE IF EXISTS " + TAB_QUIZ);
			db.execSQL("DROP TABLE IF EXISTS " + TAB_ALTERNATIVE);
			db.execSQL("DROP TABLE IF EXISTS " + TAB_VERSION);
			db.execSQL("DROP TABLE IF EXISTS " + TAB_STATISTIC);
			db.execSQL("DROP TABLE IF EXISTS " + TAB_CHALLENGE);
			onCreate(db);

		}

	}

}
