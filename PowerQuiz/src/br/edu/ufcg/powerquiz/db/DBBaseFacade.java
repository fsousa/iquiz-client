package br.edu.ufcg.powerquiz.db;

import java.util.ArrayList;

import br.edu.ufcg.powerquiz.model.Category;
import br.edu.ufcg.powerquiz.model.Historical;
import br.edu.ufcg.powerquiz.model.Module;
import br.edu.ufcg.powerquiz.model.Quiz;
import br.edu.ufcg.powerquiz.model.QuizAlternative;

import android.content.Context;
import android.database.Cursor;

public class DBBaseFacade {

	private DBAdapter db;

	public DBBaseFacade(Context ctx, String user) {

		db = new DBAdapter(ctx, user);

	}

	public void insertIntoModule(int mod_id, String name, String description,
			int author_id, int category_id, float price, int version) {
		db.open();
		db.insertIntoModule(mod_id, name, description, author_id, category_id,
				price, version);
		db.close();
	}

	public Boolean existsModuleOnDB(int mod_id) {
		db.open();
		int count = db.numberOfModulesByID(mod_id);
		if (count > 0) {
			return true;
		}
		db.close();
		return false;
	}
	
	public String retriveUpdateString() {
		db.open();
		Cursor c = db.retriveAllModuleIdAndVersion();
		StringBuilder sb = new StringBuilder();
		if (c != null && c.moveToFirst()) {
			do {
				String mod_id = c.getString(c.getColumnIndex("mod_id"));
				String version = c.getString(c.getColumnIndex("version"));
				sb.append(mod_id).append("*").append(version).append(",");
						
			}while(c.moveToNext());
		}
		db.close();
		c.close();
		return sb.toString();
	}

	public Module retriveModuleById(int modId) {
		db.open();
		Module mRetrive = null;
		Cursor c = db.retriveModuleById(modId);
		if (c != null && c.moveToFirst()) {
			do {
				String mId = c.getString(c.getColumnIndex("mod_id"));
				String name = c.getString(c.getColumnIndex("name"));
				String drescription = c.getString(c
						.getColumnIndex("description"));
				String authorId = c.getString(c.getColumnIndex("author_id"));
				String categoryId = c
						.getString(c.getColumnIndex("category_id"));
				String price = c.getString(c.getColumnIndex("price"));

				mRetrive = new Module(Integer.valueOf(mId), authorId, name,
						drescription, new Category(categoryId),
						Integer.valueOf(price));

			} while (c.moveToNext());
		}
		db.close();
		c.close();
		return mRetrive;
	}

	public ArrayList<Module> retriveAllModule() {
		db.open();
		ArrayList<Module> mList = new ArrayList<Module>();
		Cursor c = db.retriveAllModule();
		if (c != null && c.moveToFirst()) {
			do {
				String mId = c.getString(c.getColumnIndex("mod_id"));
				String name = c.getString(c.getColumnIndex("name"));
				String drescription = c.getString(c
						.getColumnIndex("description"));
				String authorId = c.getString(c.getColumnIndex("author_id"));
				String categoryId = c
						.getString(c.getColumnIndex("category_id"));
				String price = c.getString(c.getColumnIndex("price"));

				Module mRetrive = new Module(Integer.valueOf(mId), authorId,
						name, drescription, new Category(categoryId),
						Integer.valueOf(price));
				mList.add(mRetrive);

			} while (c.moveToNext());
		}
		db.close();
		c.close();
		return mList;
	}

	public void deleteModule(int mod_id) {
		db.open();
		db.deleteModule(mod_id);
		db.close();
	}

	public void insertIntoQuiz(int quiz_id, int mod_id, String question) {
		db.open();
		db.insertIntoQuiz(quiz_id, mod_id, question);
		db.close();
	}

	public ArrayList<Quiz> retriveQuizById(int quiz_id) {
		db.open();
		Cursor c = db.listQuizById(quiz_id);
		ArrayList<Quiz> lQuiz = new ArrayList<Quiz>();
		if (c != null && c.moveToFirst()) {
			do {
				int id = c.getInt(c.getColumnIndex("quiz_id"));
				int modId = c.getInt(c.getColumnIndex("mod_id"));
				String question = c.getString(c.getColumnIndex("question"));

				Quiz q = new Quiz(id, modId, question);
				lQuiz.add(q);

			} while (c.moveToNext());
		}
		db.close();
		c.close();
		return lQuiz;
	}

	public ArrayList<Quiz> retriveQuizByModule(int modId, int limit) {
		db.open();
		Cursor c = db.listQuizByModule(modId, limit);
		ArrayList<Quiz> lQuiz = new ArrayList<Quiz>();
		if (c != null && c.moveToFirst()) {
			do {
				int id = c.getInt(c.getColumnIndex("quiz_id"));
				int mod_id = c.getInt(c.getColumnIndex("mod_id"));
				String question = c.getString(c.getColumnIndex("question"));

				Quiz q = new Quiz(id, mod_id, question);
				lQuiz.add(q);

			} while (c.moveToNext());
		}
		db.close();
		c.close();
		return lQuiz;
	}

	public void insertIntoAlternative(int alternative_id, int quiz_id,
			String alternative, int correct) {
		db.open();
		db.insertIntoAlternative(alternative_id, quiz_id, alternative, correct);
		db.close();
	}

	public ArrayList<QuizAlternative> retriveAlternativeByQuiz(int quiz_id) {
		db.open();
		Cursor c = db.listAlternativesByQuiz(quiz_id);
		ArrayList<QuizAlternative> aQuiz = new ArrayList<QuizAlternative>();
		if (c != null && c.moveToFirst()) {
			do {
				int alternativeId = c
						.getInt(c.getColumnIndex("alternative_id"));
				int quizId = c.getInt(c.getColumnIndex("quiz_id"));
				String alternative = c.getString(c
						.getColumnIndex("alternative"));
				int correct = c.getInt(c.getColumnIndex("correct"));

				QuizAlternative a = new QuizAlternative(alternativeId, quizId,
						alternative, correct);
				aQuiz.add(a);

			} while (c.moveToNext());
		}
		db.close();
		c.close();
		return aQuiz;
	}

	public ArrayList<QuizAlternative> retriveAlternativeById(int alternativeId) {
		db.open();
		Cursor c = db.listQuizAlternativeById(alternativeId);
		ArrayList<QuizAlternative> aQuiz = new ArrayList<QuizAlternative>();
		if (c != null && c.moveToFirst()) {
			do {
				int alternative_id = c.getInt(c
						.getColumnIndex("alternative_id"));
				int quizId = c.getInt(c.getColumnIndex("quiz_id"));
				String alternative = c.getString(c
						.getColumnIndex("alternative"));
				int correct = c.getInt(c.getColumnIndex("correct"));

				QuizAlternative a = new QuizAlternative(alternative_id, quizId,
						alternative, correct);
				aQuiz.add(a);

			} while (c.moveToNext());
		}
		db.close();
		c.close();
		return aQuiz;
	}
	
	public void insertIntoStatistic(int mod_id, int hits, int total) {
		db.open();
		db.insertIntoStatistic(mod_id, hits, total);
		db.close();
	}
	
	public String retriveStatisticByModule(int mod_id) {
		db.open();
		Cursor c = db.listStatisticByModule(mod_id);
		StringBuilder sb = new StringBuilder();
		if (c != null && c.moveToFirst()) {
			do {
				int modId = c.getInt(c
						.getColumnIndex("mod_id"));
				int hits = c.getInt(c.getColumnIndex("hits"));
				int total = c.getInt(c
						.getColumnIndex("total"));
				String partial = String.valueOf(modId) + "," + String.valueOf(hits) + "," + String.valueOf(total);
				sb.append(partial).append("#");
				
			}while(c.moveToNext());
		}
		db.close();
		c.close();
		return sb.toString();
	}
	
	public ArrayList<Historical> retriveChallengeHistorical() {
		db.open();
		Cursor c = db.listChallengeHistorical();
		ArrayList<Historical> hList = new ArrayList<Historical>();
		if (c != null && c.moveToFirst()) {
			do {
				int challenging = c.getInt(c
						.getColumnIndex("challenging"));
				int challenged = c.getInt(c.getColumnIndex("challenged"));
				int mod_id = c.getInt(c
						.getColumnIndex("mod_id"));
				int executed = c.getInt(c
						.getColumnIndex("executed"));
				hList.add(new Historical(challenging, challenged, mod_id, executed));
				
			}while(c.moveToNext());
		}
		db.close();
		c.close();
		return hList;
	}
}
