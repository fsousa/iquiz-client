package br.edu.ufcg.powerquiz.controller;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import br.edu.ufcg.powerquiz.model.User;
import br.edu.ufcg.powerquiz.util.Cryptography;
import br.edu.ufcg.powerquiz.util.HttpRequest;

public class UserController {

	public static UserController instance;

	private final String LOGINURL = HttpRequest.SERVER + HttpRequest.WEBSERVICE
			+ "/user/getLogin.php";
	private final String REGISTERURL = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/user/register.php";
	private final String SEARCHUSER = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/user/listUser.php";
	private final String CHECKBALANCE = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/user/checkBalance.php";
	private final String GETPOINTS = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/user/getPoints.php";
	private final String GETPROFILE = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/user/profile.php";

	private UserController() {

	}

	public static UserController getInstance() {
		if (instance == null) {
			instance = new UserController();
		}
		return instance;
	}

	public User getUserProfile(int user_id) {
		String response = HttpRequest.requestHttp(String.format("%s?user_id=%s",
				GETPROFILE, user_id));
		User u = null;
		try {
			JSONArray jArray = new JSONArray(response);
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jObject = jArray.getJSONObject(i);

				String uid = jObject.getString("user_id");
				String uEmail = jObject.getString("email");
				String uName = jObject.getString("name");
				String uLastName = jObject.getString("last_name");
				String image = jObject.getString("image");
				u = new User(Integer.valueOf(uid), uName, uLastName, uEmail, image);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return u;
	}

	public String getLoginUser(String email, String pass) {
		String passwordEncrypt = Cryptography.encryptPassword(Cryptography
				.encryptPassword(pass) + email);
		String response = HttpRequest.requestHttp(String.format(
				"%s?email=%s&pass=%s", LOGINURL, email, passwordEncrypt));

		if (response.equals("false")) {
			return response;
		}
		String joinUserConf = "";
		try {
			JSONArray jArray = new JSONArray(response);
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jObject = jArray.getJSONObject(i);

				String uid = jObject.getString("user_id");
				String uEmail = jObject.getString("email");
				String uName = jObject.getString("name");
				String uLastName = jObject.getString("last_name");
				String image = jObject.getString("image");

				joinUserConf = String.format("%s#%s#%s#%s#%s#", uid, uEmail,
						uName, uLastName, image);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return joinUserConf;
	}

	public Boolean registerUser(String name, String lastName, String email,
			String password) {
		String passwordEncrypt = Cryptography.encryptPassword(Cryptography
				.encryptPassword(password) + email);
		String response = HttpRequest.requestHttp(String.format(
				"%s?name=%s&lastName=%s&email=%s&pass=%s", REGISTERURL, name,
				lastName, email, passwordEncrypt).replace(" ", "%20"));
		if (response.equals("true")) {
			return true;
		}
		return false;
	}

	public ArrayList<User> searchUser(String name) {
		String response = HttpRequest.requestHttp(String.format("%s?name=%s",
				SEARCHUSER, name));
		ArrayList<User> sUsersList = new ArrayList<User>();
		try {
			JSONArray jArray = new JSONArray(response);
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jObject = jArray.getJSONObject(i);

				String id = jObject.getString("user_id");
				String uEmail = jObject.getString("email");
				String uName = jObject.getString("name");
				String uLastName = jObject.getString("last_name");
				String image = jObject.getString("image");

				sUsersList.add(new User(Integer.valueOf(id), uName, uLastName,
						uEmail, image));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sUsersList;
	}

	public boolean checkBalance(int user_id, int mod_id) {
		String response = HttpRequest.requestHttp(String.format(
				"%s?user_id=%s&mod_id=%s", CHECKBALANCE, user_id, mod_id));
		if (response.equals("true")) {
			return true;
		}
		return false;
	}

	public String getPoints(int user_id) {
		String response = HttpRequest.requestHttp(String.format(
				"%s?user_id=%s", GETPOINTS, user_id));
		return response;
	}

}
