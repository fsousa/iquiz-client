package br.edu.ufcg.powerquiz.controller;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;
import br.edu.ufcg.powerquiz.model.Quiz;
import br.edu.ufcg.powerquiz.model.QuizAlternative;
import br.edu.ufcg.powerquiz.util.HttpRequest;

public class QuizController {

	public static QuizController instance;

	private final String LISTALLQUESTIONSBYMODULE = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/quiz/listQuizByModule.php";
	private final String LISTALLALTERNATIVESBYQUESTION = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/quiz/listAlternativesByQuiz.php";

	private QuizController() {

	}

	public static QuizController getInstance() {
		if (instance == null) {
			instance = new QuizController();
		}
		return instance;
	}

	public ArrayList<Quiz> getAllQuizByModule(String modId) {
		ArrayList<Quiz> quizes = new ArrayList<Quiz>();

		try {
			String jResponse = HttpRequest.requestHttp(LISTALLQUESTIONSBYMODULE
					+ String.format("?mod_id=%s", modId));
			JSONArray jArray = new JSONArray(jResponse);
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jObject = jArray.getJSONObject(i);

				int qid = jObject.getInt("quiz_id");
				int mid = jObject.getInt("mod_id");
				String question = jObject.getString("question");

				quizes.add(new Quiz(qid, mid, question));

			}
		} catch (Exception e) {
			Log.w("QUIZCONTROLLER",e.getMessage());
		}
		return quizes;
	}

	public ArrayList<QuizAlternative> getAllAlternativesByQuestion(int quiz_id) {
		ArrayList<QuizAlternative> alternatives = new ArrayList<QuizAlternative>();

		try {
			String jResponse = HttpRequest
					.requestHttp(LISTALLALTERNATIVESBYQUESTION
							+ String.format("?quiz_id=%s", quiz_id));
			JSONArray jArray = new JSONArray(jResponse);
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jObject = jArray.getJSONObject(i);

				int aid = jObject.getInt("alternative_id");
				String alternative = jObject.getString("alternative");
				int correct = jObject.getInt("correct");

				alternatives.add(new QuizAlternative(aid, quiz_id, alternative,
						correct));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return alternatives;
	}

}
