package br.edu.ufcg.powerquiz.controller;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import br.edu.ufcg.powerquiz.model.Category;
import br.edu.ufcg.powerquiz.model.Historical;
import br.edu.ufcg.powerquiz.model.Module;
import br.edu.ufcg.powerquiz.util.HttpRequest;

public class ModuleController {

	
	private static ModuleController instance;
	private ArrayList<Category> categoryList = new ArrayList<Category>();

	private final String LISTCATEGORY = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/module/listCategories.php";
	private final String LISTMODULESBYCAT = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/module/listModulesByCategory.php";
	private final String LISTMODULEDETAILS = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/module/listModuleDetails.php";
	private final String SEARCHMODULE = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/module/searchModule.php";
	private final String BUYMODULE = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/module/buyModule.php";
	private final String BUYCHALLENGE = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/module/buyChallenge.php";
	private final String UPDATEMODULE = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/module/checkModuleUpdate.php";

	private final String LISTCHALLENGE = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/module/listChallenge.php";
	private final String LISTCHALLENGED = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/module/listChallenged.php";
	private final String ADDCHALLENGE = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/module/addChallenge.php";
	private final String RMCHALLENGE = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/module/deleteChallenge.php";
	private final String UPDATECHALLENGE = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/module/updateChallenge.php";
	
	private final static String GETWINNER = HttpRequest.SERVER
			+ HttpRequest.WEBSERVICE + "/module/getWinner.php";

	private ModuleController() {

	}

	public static ModuleController getInstance() {
		if (instance == null) {
			instance = new ModuleController();
			return instance;
		}
		return instance;
	}

	public Module buyModule(int user_id, int mod_id) {
		Module mReturn = null;
		try {
			String jResponse = HttpRequest.requestHttp(BUYMODULE
					+ String.format("?user_id=%d&mod_id=%d", user_id, mod_id));
			JSONArray jArray = new JSONArray(jResponse);
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jObject = jArray.getJSONObject(i);

				int id = jObject.getInt("mod_id");
				int own = jObject.getInt("author_id");
				String name = jObject.getString("name");
				String description = jObject.getString("description");
				int category = jObject.getInt("category_id");
				int price = jObject.getInt("price");
				int version = jObject.getInt("version");

				mReturn = new Module(id, own, name, description, new Category(
						category), price, version);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mReturn;
	}
	
	public Module buyChallenge(int mod_id) {
		Module mReturn = null;
		try {
			String jResponse = HttpRequest.requestHttp(BUYCHALLENGE
					+ String.format("?mod_id=%d", mod_id));
			JSONArray jArray = new JSONArray(jResponse);
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jObject = jArray.getJSONObject(i);

				int id = jObject.getInt("mod_id");
				int own = jObject.getInt("author_id");
				String name = jObject.getString("name");
				String description = jObject.getString("description");
				int category = jObject.getInt("category_id");
				int price = jObject.getInt("price");
				int version = jObject.getInt("version");

				mReturn = new Module(id, own, name, description, new Category(
						category), price, version);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mReturn;
	}

	public ArrayList<Integer> checModulesUpdate(String mList) {
		String jResponse = HttpRequest.requestHttp(UPDATEMODULE
				+ String.format("?mList=%s", mList));
		ArrayList<Integer> out = new ArrayList<Integer>();

		try {
			JSONObject jArray = new JSONObject(jResponse);
			JSONArray mods = jArray.getJSONArray("modules");
			for (int i = 0; i < mods.length(); i++) {
				out.add(Integer.valueOf(mods.getString(i)));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return out;
	}

	public ArrayList<Module> listModuleByCategory(String catid) {
		ArrayList<Module> moduleList = new ArrayList<Module>();
		try {
			String jResponse = HttpRequest.requestHttp(LISTMODULESBYCAT
					+ String.format("?cat_id=%s", catid));
			JSONArray jArray = new JSONArray(jResponse);
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jObject = jArray.getJSONObject(i);

				int id = jObject.getInt("ID");
				String own = jObject.getString("AUTHOR_NAME");
				String name = jObject.getString("NAME");
				String description = jObject.getString("DESCRIPTION");
				String category = jObject.getString("CATEGORY");
				int price = jObject.getInt("PRICECE");

				Module mod = new Module(id, own, name, description,
						new Category(category), price);
				moduleList.add(mod);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return moduleList;
	}

	public ArrayList<Category> listAllCategory() {
		if (categoryList.size() == 0) {
			try {
				String jsonResponse = HttpRequest.requestHttp(LISTCATEGORY);
				JSONArray jArray = new JSONArray(jsonResponse);

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jObject = jArray.getJSONObject(i);
					Category cat = new Category(jObject.getInt("cat_id"),
							jObject.getString("name"));
					this.categoryList.add(cat);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return categoryList;
	}

	public ArrayList<Module> searchModule(String search) {
		ArrayList<Module> searchResultList = new ArrayList<Module>();
		try {
			String jsonResponse = HttpRequest.requestHttp(SEARCHMODULE
					+ String.format("?q=%s", search));
			JSONArray jArray = new JSONArray(jsonResponse);

			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jObject = jArray.getJSONObject(i);

				int id = jObject.getInt("ID");
				String own = jObject.getString("AUTHOR_NAME");
				String name = jObject.getString("NAME");
				String description = jObject.getString("DESCRIPTION");
				String category = jObject.getString("CATEGORY");
				int price = jObject.getInt("PRICE");

				Module mod = new Module(id, own, name, description,
						new Category(category), price);
				searchResultList.add(mod);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return searchResultList;

	}

	public ArrayList<Historical> downloadChallenge(int user_id) {
		ArrayList<Historical> lHistorical = new ArrayList<Historical>();
		try {
			String result = HttpRequest.requestHttp(LISTCHALLENGE
					+ String.format("?user_id=%s", String.valueOf(user_id)));
			JSONArray jArray = new JSONArray(result);

			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jObject = jArray.getJSONObject(i);
				int challenging = jObject.getInt("challenging");
				int challenged = jObject.getInt("challenged");
				int mod_id = jObject.getInt("mod_id");
				int executed = jObject.getInt("executed");
				lHistorical
						.add(new Historical(challenging, challenged, mod_id, executed));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lHistorical;
	}

	public ArrayList<Historical> downloadChallenged(int user_id) {
		ArrayList<Historical> lHistorical = new ArrayList<Historical>();
		try {
			String result = HttpRequest.requestHttp(LISTCHALLENGED
					+ String.format("?user_id=%s", String.valueOf(user_id)));
			JSONArray jArray = new JSONArray(result);

			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jObject = jArray.getJSONObject(i);
				int challenging = jObject.getInt("challenging");
				int challenged = jObject.getInt("challenged");
				int mod_id = jObject.getInt("mod_id");
				int executed = jObject.getInt("executed");
				lHistorical
						.add(new Historical(challenging, challenged, mod_id, executed));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lHistorical;
	}

	public boolean addChallenge(int user1, int user2, int mod_id) {
		boolean out = false;
		try {
			String result = HttpRequest.requestHttp(ADDCHALLENGE
					+ String.format("?user1=%s&user2=%s&mod_id=%s",
							String.valueOf(user1), String.valueOf(user2),
							String.valueOf(mod_id)));
			if (result.equals("true")) {
				out = true;
			} else {
				out = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return out;
	}

	public void deleteChallenge(int user1, int user2, int mod_id) {
		try {
			HttpRequest.requestHttp(RMCHALLENGE
					+ String.format("?user1=%s&user2=%s&mod_id=%s",
							String.valueOf(user1), String.valueOf(user2),
							String.valueOf(mod_id)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateChallenge(int user1, int user2, int mod_id, int executed) {
		try {
			HttpRequest.requestHttp(UPDATECHALLENGE
					+ String.format("?user1=%s&user2=%s&mod_id=%s&executed=%s",
							String.valueOf(user1), String.valueOf(user2),
							String.valueOf(mod_id), String.valueOf(executed)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Module moduleDetails(int mod_id) {
		Module mod = null;
		try {
			String jResponse = HttpRequest.requestHttp(LISTMODULEDETAILS
					+ String.format("?mod_id=%s", mod_id));
			JSONArray jArray = new JSONArray(jResponse);
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jObject = jArray.getJSONObject(i);

				int id = jObject.getInt("ID");
				String own = jObject.getString("AUTHOR_NAME");
				String name = jObject.getString("NAME");
				String description = jObject.getString("DESCRIPTION");
				String category = jObject.getString("CATEGORY");
				int price = jObject.getInt("PRICE");

				mod = new Module(id, own, name, description,
						new Category(category), price);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mod;
	}

	public String getChallengeWinner(int user1, int user2, int mod_id) {
		String winner = "";
		try {
			String jResponse = HttpRequest.requestHttp(GETWINNER
					+ String.format("?user1=%s&user2=%s&mod_id=%s", user1, user2, mod_id));
			JSONArray jArray = new JSONArray(jResponse);
			JSONObject jObject = jArray.getJSONObject(0);
			winner = jObject.getString("name") + " " + jObject.getString("last_name");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return winner;
	}
}
