package br.edu.ufcg.powerquiz.controller;

import br.edu.ufcg.powerquiz.model.Play;
import br.edu.ufcg.powerquiz.model.PlayModule;

public class PlayController {
	
	private static PlayController instance;

	
	private PlayController() {
		
	}
	
	public static PlayController getInstance() {
		if (instance == null) {
			instance = new PlayController();
			return instance;
		}
		return instance;
	}
	
	public Play createPlayGame(PlayModule module) {
		return new Play(module);
	}

}
