package br.edu.ufcg.powerquiz.util;

import java.util.ArrayList;

import android.content.Context;
import br.edu.ufcg.powerquiz.controller.ModuleController;
import br.edu.ufcg.powerquiz.controller.QuizController;
import br.edu.ufcg.powerquiz.db.DBBaseFacade;
import br.edu.ufcg.powerquiz.model.Module;
import br.edu.ufcg.powerquiz.model.Quiz;
import br.edu.ufcg.powerquiz.model.QuizAlternative;

public class DownloadModule {
	
	private ArrayList<Quiz> myQuizes;
	private DBBaseFacade dbSqlite;
	
	private QuizController qController = QuizController.getInstance();
	private ModuleController mController = ModuleController.getInstance();
	private Context ctx;
	public Context getCtx() {
		return ctx;
	}

	public void setCtx(Context ctx) {
		this.ctx = ctx;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	private String email;
	
	public DownloadModule(Context ctx, String email) {
		this.ctx = ctx;
		this.email = email;
	}
	
	public void downInsertModule(int user_id, int mod_id) {
		Module mBuy = mController.buyModule(user_id, mod_id);
		dbSqlite = new DBBaseFacade(this.ctx, this.email);
		dbSqlite.insertIntoModule(mBuy.getId(), mBuy.getName(), mBuy.getDescription(), mBuy.getOwnId(), mBuy.getCategory().getId(), mBuy.getPrice(), mBuy.getVersion());
		myQuizes = qController.getAllQuizByModule(String.valueOf(mod_id));
		for (Quiz quiz : myQuizes) {
			dbSqlite.insertIntoQuiz(quiz.getId(), quiz.getModId(), quiz.getQuestion());
			ArrayList<QuizAlternative> myAlternatives = qController.getAllAlternativesByQuestion(quiz.getId());
			for (QuizAlternative quizAlternative : myAlternatives) {
				dbSqlite.insertIntoAlternative(quizAlternative.getId(), quizAlternative.getQuizId(), quizAlternative.getAlternative(), quizAlternative.getCorrect());
			}
		}
	}
	
	public void downInsertChallenge(int mod_id) {
		Module mBuy = mController.buyChallenge(mod_id);
		dbSqlite = new DBBaseFacade(this.ctx, this.email);
		dbSqlite.insertIntoModule(mBuy.getId(), mBuy.getName(), mBuy.getDescription(), mBuy.getOwnId(), mBuy.getCategory().getId(), mBuy.getPrice(), mBuy.getVersion());
		myQuizes = qController.getAllQuizByModule(String.valueOf(mod_id));
		for (Quiz quiz : myQuizes) {
			dbSqlite.insertIntoQuiz(quiz.getId(), quiz.getModId(), quiz.getQuestion());
			ArrayList<QuizAlternative> myAlternatives = qController.getAllAlternativesByQuestion(quiz.getId());
			for (QuizAlternative quizAlternative : myAlternatives) {
				dbSqlite.insertIntoAlternative(quizAlternative.getId(), quizAlternative.getQuizId(), quizAlternative.getAlternative(), quizAlternative.getCorrect());
			}
		}
	}

}
