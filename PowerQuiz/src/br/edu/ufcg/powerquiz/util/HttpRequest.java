package br.edu.ufcg.powerquiz.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;

public class HttpRequest {

	// public static final String SERVER = "http://projeto1-felipe.embedded.ufcg.edu.br";
	public static final String SERVER = "http://192.168.15.143";
	public static final String WEBSERVICE = "/iquiz/PowerQuiz-Server/webservice";

	public static String requestHttp(String address) {
		String urlContent = "";
		try {
			URL url = new URL(address);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			urlContent += readStream(con.getInputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return urlContent;
	}

	private static String readStream(InputStream in) throws IOException {
		String out = "";
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String line = "";
		while ((line = reader.readLine()) != null) {
			out += line;
		}
		return out;
	}

	public static Bitmap loadBitmap(String imageUrl) {
		Bitmap bitmap = null;
		try {
			bitmap = BitmapFactory.decodeStream((InputStream) new URL(
					imageUrl).getContent());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bitmap;
	}
	
	public static Drawable getProfileImage(String url, String src_name) {
		Drawable d = null;
		try {
			 d = Drawable.createFromStream(((java.io.InputStream)
				      new java.net.URL(url).getContent()), src_name);
			 
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return d;
	}
}
