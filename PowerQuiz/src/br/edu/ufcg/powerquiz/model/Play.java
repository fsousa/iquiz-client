package br.edu.ufcg.powerquiz.model;

public class Play {
	
	private PlayModule module;
	
	public Play(PlayModule module) {
		this.module = module;
	}

	public PlayModule getModule() {
		return module;
	}

	public void setModule(PlayModule module) {
		this.module = module;
	}
	
	
}
