package br.edu.ufcg.powerquiz.model;

import java.util.ArrayList;

public class PlayModule {
	
	private Module module;
	private ArrayList<PlayQuiz> quizes;
	
	public PlayModule(Module module) {
		this.module = module;
		this.quizes = new ArrayList<PlayQuiz>();
	}
	
	public void addQuiz(PlayQuiz pQuiz) {
		this.quizes.add(pQuiz);
	}
	
	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public ArrayList<PlayQuiz> getQuizes() {
		return quizes;
	}

	public void setQuizes(ArrayList<PlayQuiz> quizes) {
		this.quizes = quizes;
	}
	
	

}
