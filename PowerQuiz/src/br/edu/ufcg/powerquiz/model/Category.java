package br.edu.ufcg.powerquiz.model;

public class Category {
	
	private int id;
	private String name;
	
	public Category(int id, String name) {
		setId(id);
		setName(name);
	}
	
	public Category(String name) {
		setId(-1);
		setName(name);
	}
	
	public Category(int id) {
		setId(id);
		setName("");
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return getName();
	}

}
