package br.edu.ufcg.powerquiz.model;

public class Historical {
	
	private int user1;
	private int user2;
	private int mod_id;
	private int executed;
	
	public Historical(int user1, int user2, int mod_id, int executed) {
		this.user1 = user1;
		this.user2 = user2;
		this.mod_id = mod_id;
		this.setExecuted(executed);
	}

	public int getUser1() {
		return user1;
	}

	public void setUser1(int user1) {
		this.user1 = user1;
	}

	public int getUser2() {
		return user2;
	}

	public void setUser2(int user2) {
		this.user2 = user2;
	}

	public int getMod_id() {
		return mod_id;
	}

	public void setMod_id(int mod_id) {
		this.mod_id = mod_id;
	}

	public int getExecuted() {
		return executed;
	}

	public void setExecuted(int executed) {
		this.executed = executed;
	}
	

}
