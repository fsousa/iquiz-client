package br.edu.ufcg.powerquiz.model;

public class User {
	
	private int id;
	private String name;
	private String lastName;
	private String email;
	private String picture;
	
	public User(int id, String name, String lastName, String email, String picture) {
		setId(id);
		setName(name);
		setLastName(lastName);
		setEmail(email);
		setPicture(picture);
	}
	
	public User(String sharedStorage) {
		String[] splited = sharedStorage.split("#");
		
		setId(Integer.parseInt(splited[0]));
		setEmail(splited[1]);
		setName(splited[2]);
		setLastName(splited[3]);
		setPicture(splited[4]);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
	
	@Override
	public boolean equals(Object other){
	    if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof User))return false;
	    User otherMyClass = (User)other;
	    if(this.getId() == otherMyClass.getId())  return true;
	    return false;
	}

}
