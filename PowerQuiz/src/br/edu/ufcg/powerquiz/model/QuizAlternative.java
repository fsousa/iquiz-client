package br.edu.ufcg.powerquiz.model;

public class QuizAlternative {
	
	private int id;
	private int quizId;
	private String alternative;
	private int correct;
	
	public QuizAlternative(int id, int quizId, String alternative, int correct) {
		setId(id);
		setQuizId(quizId);
		setAlternative(alternative);
		setCorrect(correct);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuizId() {
		return quizId;
	}

	public void setQuizId(int quizId) {
		this.quizId = quizId;
	}

	public String getAlternative() {
		return alternative;
	}

	public void setAlternative(String alternative) {
		this.alternative = alternative;
	}

	public int getCorrect() {
		return correct;
	}

	public void setCorrect(int correct) {
		this.correct = correct;
	}

}
