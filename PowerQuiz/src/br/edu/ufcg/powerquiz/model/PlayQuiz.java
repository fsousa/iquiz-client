package br.edu.ufcg.powerquiz.model;

import java.util.ArrayList;

public class PlayQuiz {

	private Quiz quiz;

	private ArrayList<QuizAlternative> alternatives;

	public PlayQuiz(Quiz quiz, ArrayList<QuizAlternative> alternatives) {
		this.quiz = quiz;
		this.alternatives = alternatives;
	}

	public Quiz getQuiz() {
		return quiz;
	}

	public void setQuiz(Quiz quiz) {
		this.quiz = quiz;
	}

	public ArrayList<QuizAlternative> getAlternatives() {
		return alternatives;
	}

	public void setAlternatives(ArrayList<QuizAlternative> alternatives) {
		this.alternatives = alternatives;
	}
}
