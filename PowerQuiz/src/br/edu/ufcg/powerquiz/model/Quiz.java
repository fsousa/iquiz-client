package br.edu.ufcg.powerquiz.model;

import java.util.ArrayList;

public class Quiz {

	private int id;
	private int modId;
	private String question;
	private ArrayList<QuizAlternative> alternatives = new ArrayList<QuizAlternative>();

	public Quiz(int id, int modId, String question) {
		setId(id);
		setModId(modId);
		setQuestion(question);
	}
	
	public void addAlternative(QuizAlternative alter) {
		getAlternatives().add(alter);
	}
	
	public void removeAlternative(QuizAlternative alter) {
		getAlternatives().remove(alter);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getModId() {
		return modId;
	}

	public void setModId(int modId) {
		this.modId = modId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public ArrayList<QuizAlternative> getAlternatives() {
		return alternatives;
	}

	public void setAlternatives(ArrayList<QuizAlternative> alternatives) {
		this.alternatives = alternatives;
	}

}
