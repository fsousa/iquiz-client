package br.edu.ufcg.powerquiz.model;

import java.util.ArrayList;

public class Module {
	
	private int id;
	private String own;
	private int ownId;
	private String name;
	private String description;
	private Category category;
	private int price;
	private int version;
	private ArrayList<Quiz> quizes = new ArrayList<Quiz>();
	
	public Module(int id, String own, String name, String description, Category category, int price) {
		setCategory(category);
		setDescription(description);
		setId(id);
		setName(name);
		setOwn(own);
		setPrice(price);
		
	}
	
	public Module(int id2, int own2, String name2, String description2,
			Category category2, int price2, int version) {
		setCategory(category2);
		setDescription(description2);
		setId(id2);
		setName(name2);
		setOwnId(own2);
		setPrice(price2);
		setVersion(version);
	}

	public void addQuiz(Quiz quiz) {
		getQuizes().add(quiz);
	}
	
	public void removeQuiz(Quiz quiz) {
		getQuizes().remove(quiz);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOwn() {
		return own;
	}
	public void setOwn(String own) {
		this.own = own;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public ArrayList<Quiz> getQuizes() {
		return quizes;
	}
	public void setQuizes(ArrayList<Quiz> quizes) {
		this.quizes = quizes;
	}


	public int getOwnId() {
		return ownId;
	}


	public void setOwnId(int ownId) {
		this.ownId = ownId;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}

	public int getVersion() {
		return this.version;
	}

}
