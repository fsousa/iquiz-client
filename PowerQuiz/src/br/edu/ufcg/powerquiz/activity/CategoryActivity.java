package br.edu.ufcg.powerquiz.activity;

import java.util.ArrayList;

import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.controller.ModuleController;
import br.edu.ufcg.powerquiz.model.Category;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class CategoryActivity extends Activity {

	ModuleController mController = ModuleController.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category);
		ListView listViewCategory = (ListView) findViewById(R.id.category_listView);

		final ArrayList<Category> lCategory = mController.listAllCategory();

		MyCategoryArrayAdapter adapter = new MyCategoryArrayAdapter(
				getApplicationContext(), lCategory);

		listViewCategory.setAdapter(adapter);

		listViewCategory.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				Intent myIntent = new Intent(CategoryActivity.this,
						ModuleListActivity.class);
				myIntent.putExtra("cat_id", Integer.toString(lCategory.get(position).getId()));
				CategoryActivity.this.startActivity(myIntent);

			}
		});
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	        finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.category, menu);
		return true;
	}

	private class MyCategoryArrayAdapter extends ArrayAdapter<Category> {
		private final Context context;
		private final ArrayList<Category> values;

		public MyCategoryArrayAdapter(Context context,
				ArrayList<Category> values) {
			super(context, R.layout.category_item, values);
			this.context = context;
			this.values = values;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.category_item, parent,
					false);
			TextView textView = (TextView) rowView
					.findViewById(R.id.textViewCategoryName);
			textView.setText(values.get(position).getName());

			return rowView;
		}
	}

}
