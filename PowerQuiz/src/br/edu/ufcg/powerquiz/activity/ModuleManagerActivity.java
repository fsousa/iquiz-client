package br.edu.ufcg.powerquiz.activity;

import java.util.ArrayList;

import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.controller.ModuleController;
import br.edu.ufcg.powerquiz.controller.QuizController;
import br.edu.ufcg.powerquiz.db.DBBaseFacade;
import br.edu.ufcg.powerquiz.db.DBFlash;
import br.edu.ufcg.powerquiz.model.Module;
import br.edu.ufcg.powerquiz.model.Quiz;
import br.edu.ufcg.powerquiz.model.QuizAlternative;
import br.edu.ufcg.powerquiz.model.User;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class ModuleManagerActivity extends Activity {
	
	private DBBaseFacade dbSqlite;
	private DBFlash dbflash = DBFlash.getInstace();
	private ArrayList<Module> lModule;
	private MyModuleArrayAdapter adapter;
	private ModuleController mController = ModuleController.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_module_manager);
		
		ListView listViewCategory = (ListView) findViewById(R.id.listViewModuleManager);
		String uSave = dbflash.retriveConfigDB(getApplicationContext(), DBFlash.USERCONF);
		User newUser = new User(uSave);
		dbSqlite = new DBBaseFacade(getApplicationContext(), newUser.getEmail());
		
		lModule = dbSqlite.retriveAllModule();
		adapter = new MyModuleArrayAdapter(
				getApplicationContext(), lModule);
		
		Button btnUpdate = (Button) findViewById(R.id.btnCheckUpdate);
		
		btnUpdate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SyncIncoData updateModule = new SyncIncoData();
				updateModule.execute(new String[] {});
				
			}
		});
		
		View emptyView = findViewById(R.id.textViewNoModules);
		listViewCategory.setEmptyView(emptyView);
		listViewCategory.setAdapter(adapter);
		
		listViewCategory.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				
				Intent myIntent = new Intent(ModuleManagerActivity.this,
						ModuleDetailsActivity.class);
				myIntent.putExtra("moduleName", lModule.get(position).getName());
				myIntent.putExtra("des", lModule.get(position).getDescription());
				myIntent.putExtra("modId", Integer.toString(lModule.get(position).getId()));
				myIntent.putExtra("button", "remove");
				ModuleManagerActivity.this.startActivity(myIntent);
			}
		});
		
		listViewCategory.setOnItemLongClickListener(new OnItemLongClickListener() {


			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int position, long id) {
				final int itemDeleted = position;
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ModuleManagerActivity.this);
                alertDialogBuilder.setTitle("Delete Module");
                alertDialogBuilder
                    .setMessage("Are your sure ?")
                    .setCancelable(true);
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {


					@Override
					public void onClick(DialogInterface dialog, int which) {
						dbSqlite.deleteModule(adapter.getItem(itemDeleted).getId());
						adapter.remove(adapter.getItem(itemDeleted));
						adapter.notifyDataSetChanged();
						
					}
				});
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

				return false;
			}
        });
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.module_manager, menu);
		return true;
	}
	
	private void reloadListView() {
		lModule = dbSqlite.retriveAllModule();
		adapter.clear();
		adapter.addAll(lModule);
		adapter.notifyDataSetChanged();
	}
	
	@Override
	protected void onResume() {
		reloadListView();
		super.onResume();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	        finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	private class SyncIncoData extends AsyncTask<String, String, String> {

		ProgressDialog dialog;
		
		@Override
		protected String doInBackground(String... params) {
			ArrayList<Integer> response = mController.checModulesUpdate(dbSqlite.retriveUpdateString());
			if(response.size() == 0) {
				return "No update available";
			}
			QuizController qController = QuizController.getInstance();
			
			DBFlash dbflash = DBFlash.getInstace();
			String uSave = dbflash.retriveConfigDB(getApplicationContext(), DBFlash.USERCONF);
			User newUser = new User(uSave);
			
			for (Integer modId : response) {
				dbSqlite.deleteModule(modId);
				Module mBuy = mController.buyModule(newUser.getId(), modId);
				dbSqlite.insertIntoModule(mBuy.getId(), mBuy.getName(), mBuy.getDescription(), mBuy.getOwnId(), mBuy.getCategory().getId(), mBuy.getPrice(), mBuy.getVersion());
				ArrayList<Quiz>myQuizes = qController.getAllQuizByModule(String.valueOf(modId));
				for (Quiz quiz : myQuizes) {
					dbSqlite.insertIntoQuiz(quiz.getId(), quiz.getModId(), quiz.getQuestion());
					ArrayList<QuizAlternative> myAlternatives = qController.getAllAlternativesByQuestion(quiz.getId());
					for (QuizAlternative quizAlternative : myAlternatives) {
						dbSqlite.insertIntoAlternative(quizAlternative.getId(), quizAlternative.getQuizId(), quizAlternative.getAlternative(), quizAlternative.getCorrect());
					}
				}
			}
			return "Updated modules";
		}
		@Override
		protected void onPreExecute() {

			super.onPreExecute();

			dialog = ProgressDialog.show(ModuleManagerActivity.this, "", 
                    "Loading. Please wait...", true);

		}

		@Override
		protected void onPostExecute(String response) {

			dialog.dismiss();
			reloadListView();
			Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
		}
		
	}
	
}
