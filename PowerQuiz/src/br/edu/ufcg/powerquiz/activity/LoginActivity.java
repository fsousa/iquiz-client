package br.edu.ufcg.powerquiz.activity;

import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.controller.UserController;
import br.edu.ufcg.powerquiz.db.DBFlash;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {

	private UserController uController = UserController.getInstance();
	private DBFlash dbFlash = DBFlash.getInstace();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_activity);

		int SDK_INT = android.os.Build.VERSION.SDK_INT;
		if (SDK_INT > 8) {

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();

			StrictMode.setThreadPolicy(policy);

		}
		
		String uSave = dbFlash.retriveConfigDB(getApplicationContext(), DBFlash.USERREMEMBER);
		if(uSave != null && uSave != "") {
			dbFlash.saveConfigDB(uSave, getApplicationContext(),
					DBFlash.USERCONF);
			Intent myIntent = new Intent(LoginActivity.this,
					MainActivity.class);
			LoginActivity.this.startActivity(myIntent);
			finish();
		}
		

		Button button = (Button) findViewById(R.id.loginBtn);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				EditText email = (EditText) findViewById(R.id.emailEditText);
				EditText pass = (EditText) findViewById(R.id.passEditText);
				
				SyncIncoData aSync = new SyncIncoData();
				aSync.execute(new String[] {email.getText().toString(), pass.getText().toString()});

			}
		});
		
		Button btnRegister = (Button) findViewById(R.id.btnRegister);
		btnRegister.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(LoginActivity.this,
						RegisterActivity.class);
				LoginActivity.this.startActivity(myIntent);
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login_active, menu);
		return true;
	}
	
	private class SyncIncoData extends AsyncTask<String, String, String> {

		ProgressDialog dialog;
		
		@Override
		protected String doInBackground(String... params) {
			String response = uController.getLoginUser(params[0], params[1]);
			return response;
		}
		@Override
		protected void onPreExecute() {

			super.onPreExecute();

			dialog = ProgressDialog.show(LoginActivity.this, "", 
                    "Loading. Please wait...", true);

		}

		@Override
		protected void onPostExecute(String response) {

			dialog.dismiss();
			if (!response.equals("false")) {
				dbFlash.saveConfigDB(response, getApplicationContext(),
						DBFlash.USERCONF);
				CheckBox rmbme = (CheckBox) findViewById(R.id.checkBoxRemeber);
				if(rmbme.isChecked()) {
					dbFlash.saveConfigDB(response, getApplicationContext(),
							DBFlash.USERREMEMBER);
				}
				
				Intent myIntent = new Intent(LoginActivity.this,
						MainActivity.class);
				LoginActivity.this.startActivity(myIntent);
				finish();
			} else {
				Toast.makeText(getApplicationContext(),
						"Username or Password invalid. Try again.",
						Toast.LENGTH_SHORT).show();
			}

		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
		}
		
	}

}
