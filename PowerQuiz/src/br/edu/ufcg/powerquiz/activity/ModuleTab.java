package br.edu.ufcg.powerquiz.activity;

import br.edu.ufcg.powerquiz.R;
import android.os.Bundle;
import android.app.TabActivity;
import android.content.Intent;
import android.view.Menu;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

@SuppressWarnings("deprecation")
public class ModuleTab extends TabActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_module_tab);
		
		TabHost tabHost = getTabHost(); 
       
		TabSpec newQuizSpec = tabHost.newTabSpec("New Quiz");
		newQuizSpec.setIndicator("New Quiz", getResources().getDrawable(R.drawable.com_facebook_button_check));
        Intent NewQuizIntent = new Intent(this, StartQuizActicity.class);
        newQuizSpec.setContent(NewQuizIntent);
        
        TabSpec challengesSpec = tabHost.newTabSpec("Challenges");
        challengesSpec.setIndicator("Challenges", getResources().getDrawable(R.drawable.com_facebook_button_check));
        Intent photosIntent = new Intent(this, ChallengesRecivedActivity.class);
        challengesSpec.setContent(photosIntent);
        
        tabHost.addTab(newQuizSpec);
        tabHost.addTab(challengesSpec);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.module_tab, menu);
		return true;
	}

}
