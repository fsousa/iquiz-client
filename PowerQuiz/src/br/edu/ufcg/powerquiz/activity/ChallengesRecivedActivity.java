package br.edu.ufcg.powerquiz.activity;

import java.util.ArrayList;

import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.controller.ModuleController;
import br.edu.ufcg.powerquiz.controller.UserController;
import br.edu.ufcg.powerquiz.db.DBBaseFacade;
import br.edu.ufcg.powerquiz.db.DBFlash;
import br.edu.ufcg.powerquiz.model.Historical;
import br.edu.ufcg.powerquiz.model.User;
import br.edu.ufcg.powerquiz.util.DownloadImageTask;
import br.edu.ufcg.powerquiz.util.DownloadModule;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class ChallengesRecivedActivity extends Activity {

	private ModuleController mController = ModuleController.getInstance();
	private UserController uController = UserController.getInstance();
	private ArrayList<Historical> lcRevicedList;
	private DBFlash dbflash = DBFlash.getInstace();
	private DBBaseFacade dbSqlite;
	User newUser;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_challenges_recived);

		String uSave = dbflash.retriveConfigDB(getApplicationContext(),
				DBFlash.USERCONF);
		newUser = new User(uSave);
		dbSqlite = new DBBaseFacade(getApplicationContext(), newUser.getEmail());
		lcRevicedList = retriveHistoricalList(newUser.getId());
		ListView lvHistorical = (ListView) findViewById(R.id.listViewcReviced);
		MycRevicedArrayAdapter adapter = new MycRevicedArrayAdapter(
				getApplicationContext(), lcRevicedList);
		lvHistorical.setAdapter(adapter);
		lvHistorical.setEmptyView(findViewById(R.id.textViewcRevicedNoItems));

		lvHistorical.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long id) {
				
				DownloadChallengeAsync downCha = new DownloadChallengeAsync();
				downCha.execute(String.valueOf(position));
			}
		});
	}

	private ArrayList<Historical> retriveHistoricalList(int user_id) {
		return mController.downloadChallenged(user_id);
	}
	
	private class DownloadChallengeAsync extends AsyncTask<String, String, String> {

		ProgressDialog dialog;
		
		@Override
		protected String doInBackground(String... params) {
			
			if(!dbSqlite.existsModuleOnDB(lcRevicedList.get(Integer.valueOf(params[0])).getMod_id())) {
				DownloadModule dm = new DownloadModule(ChallengesRecivedActivity.this, newUser.getEmail());
				dm.downInsertChallenge(lcRevicedList.get(Integer.valueOf(params[0])).getMod_id());
				return params[0] + "#1";
			}
			
			return params[0]+ "#0";
		}
		@Override
		protected void onPreExecute() {

			super.onPreExecute();

			dialog = ProgressDialog.show(ChallengesRecivedActivity.this, "", 
                    "Loading. Please wait...", true);

		}

		@Override
		protected void onPostExecute(String response) {

			dialog.dismiss();
			Intent myIntent = new Intent(ChallengesRecivedActivity.this,
					PlayQuizActivity.class);
			String[] result = response.split("#");
			myIntent.putExtra("mod_id", String.valueOf(lcRevicedList.get(Integer.valueOf(result[0])).getMod_id()));
			myIntent.putExtra("semantic", result[1]);
			myIntent.putExtra("typeEnd", "extra");
			ChallengesRecivedActivity.this.startActivity(myIntent);

		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.historical, menu);
		return true;
	}

	private class MycRevicedArrayAdapter extends ArrayAdapter<Historical> {
		private final Context context;
		private final ArrayList<Historical> values;

		public MycRevicedArrayAdapter(Context context,
				ArrayList<Historical> values) {
			super(context, R.layout.recive_challenged_item, values);
			this.context = context;
			this.values = values;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.recive_challenged_item,
					parent, false);

			ImageView iProfile = (ImageView) rowView
					.findViewById(R.id.imageViewHistoricalPhoto);
			ImageView arrow = (ImageView) rowView
					.findViewById(R.id.imageViewArrow);

			TextView name = (TextView) rowView
					.findViewById(R.id.textViewHistoricalName);
			TextView moduleName = (TextView) rowView
					.findViewById(R.id.textViewRecivedModuleName);

			User u = uController
					.getUserProfile(values.get(position).getUser1());
			if(values.get(position).getExecuted() == 0) {
				arrow.setImageDrawable(getResources().getDrawable(
					R.drawable.arrow_down));
			}else {
				arrow.setImageDrawable(getResources().getDrawable(
						R.drawable.arrow_down_gray));
			}

			name.setText(u.getName() + " " + u.getLastName());
			moduleName.setText(mController.moduleDetails(
					values.get(position).getMod_id()).getName());

			DownloadImageTask dImage = new DownloadImageTask(iProfile);
			dImage.execute(u.getPicture());

			return rowView;
		}
	}

}
