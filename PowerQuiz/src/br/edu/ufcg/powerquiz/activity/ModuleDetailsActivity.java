package br.edu.ufcg.powerquiz.activity;

import java.util.ArrayList;

import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.controller.QuizController;
import br.edu.ufcg.powerquiz.controller.UserController;
import br.edu.ufcg.powerquiz.db.DBBaseFacade;
import br.edu.ufcg.powerquiz.db.DBFlash;
import br.edu.ufcg.powerquiz.model.Quiz;
import br.edu.ufcg.powerquiz.model.User;
import br.edu.ufcg.powerquiz.util.DownloadModule;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ModuleDetailsActivity extends Activity {
	
	private UserController uController = UserController.getInstance();
	private DBFlash dbflash = DBFlash.getInstace();
	private QuizController qController = QuizController.getInstance();
	private ArrayList<Quiz> myQuizes;
	private DBBaseFacade dbSqlite;
	private String modId;
	private boolean bSemantic;
	private Button btnDownload;
	private User newUser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_module_details);
		String uSave = dbflash.retriveConfigDB(getApplicationContext(), DBFlash.USERCONF);
		newUser = new User(uSave);
		dbSqlite = new DBBaseFacade(getApplicationContext(), newUser.getEmail());
		Intent myIntente = getIntent();
		String moduleName = myIntente.getStringExtra("moduleName");
		String desName = myIntente.getStringExtra("des");
		modId = myIntente.getStringExtra("modId");
		bSemantic = dbSqlite.existsModuleOnDB(Integer.valueOf(modId));
		
		TextView modName = (TextView) findViewById(R.id.textViewModName);
		TextView des = (TextView) findViewById(R.id.textViewDesc);
		
		modName.setText(moduleName);
		des.setText(desName);
		
		myQuizes = qController.getAllQuizByModule(modId);
		
		ListView lView = (ListView) findViewById(R.id.listViewQuiz);
		
		MyQuizArrayAdapter adapter = new MyQuizArrayAdapter(getApplicationContext(), myQuizes);
		
		View emptyView = findViewById(R.id.quizEmpty);
		lView.setEmptyView(emptyView);
		lView.setAdapter(adapter);
		
		btnDownload = (Button) findViewById(R.id.btnDownload);
		
		if(bSemantic) {
			btnDownload.setText("Remove Module");
		}
		
		btnDownload.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				String uSave = dbflash.retriveConfigDB(getApplicationContext(), DBFlash.USERCONF);
				User newUser = new User(uSave);
				
				SyncIncoData aSync = new SyncIncoData();
				aSync.execute(new String[] {String.valueOf(newUser.getId()), modId});
			}
		});
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	        finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.module_details, menu);
		return true;
	}
	
	
	
	private class MyQuizArrayAdapter extends ArrayAdapter<Quiz> {
		private final Context context;
		private final ArrayList<Quiz> values;

		public MyQuizArrayAdapter(Context context, ArrayList<Quiz> values) {
			super(context, R.layout.module_details_item, values);
			this.context = context;
			this.values = values;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.module_details_item, parent,
					false);
			
			TextView textViewAuthor = (TextView) rowView.findViewById(R.id.textViewQuizName);
			textViewAuthor.setText(values.get(position).getQuestion());
			
			return rowView;
		}
	}
	
	private class SyncIncoData extends AsyncTask<String, String, String> {

		ProgressDialog dialog;
		
		@Override
		protected String doInBackground(String... params) {
			
			int user_id = Integer.valueOf(params[0]);
			int mod_id = Integer.valueOf(params[1]);
			
			if(bSemantic) {
				dbSqlite.deleteModule(mod_id);
				finish();
			}else{
				if(uController.checkBalance(user_id, mod_id)) {
					DownloadModule dm = new DownloadModule(ModuleDetailsActivity.this, newUser.getEmail());
					dm.downInsertModule(user_id, mod_id);
				} else {
					return "false";
				}
			}
			return "";
		}

		
		@Override
		protected void onPreExecute() {

			super.onPreExecute();

			dialog = ProgressDialog.show(ModuleDetailsActivity.this, "", 
                    "Loading. Please wait...", true);

		}

		@Override
		protected void onPostExecute(String response) {
			
			dialog.dismiss();
			if(response.equals("false")) {
				Toast.makeText(getApplicationContext(), "Insufficient points", Toast.LENGTH_LONG).show();
			}else {
				bSemantic = true;
				btnDownload.setText("Remove Module");
			}
		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
		}
		
	}

}
