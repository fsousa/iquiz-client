package br.edu.ufcg.powerquiz.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.controller.PlayController;
import br.edu.ufcg.powerquiz.db.DBBaseFacade;
import br.edu.ufcg.powerquiz.db.DBFlash;
import br.edu.ufcg.powerquiz.model.Module;
import br.edu.ufcg.powerquiz.model.Play;
import br.edu.ufcg.powerquiz.model.PlayModule;
import br.edu.ufcg.powerquiz.model.PlayQuiz;
import br.edu.ufcg.powerquiz.model.Quiz;
import br.edu.ufcg.powerquiz.model.QuizAlternative;
import br.edu.ufcg.powerquiz.model.User;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
 
public class PlayQuizActivity extends Activity{
	
	private DBBaseFacade dbSqlite;
	private DBFlash dbflash = DBFlash.getInstace();
	private PlayController pController = PlayController.getInstance();
	private int index = 0;
	private TextView textQuestion;
	private Play play;
	private TextView seq;
	private MyAlternativeArrayAdapter adapter;
	private ListView lAlternative;
	private ArrayList<Integer> selected;
	private Button btnNext;
	private String semantic;
	private String typeEnd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play_quiz);
		String uSave = dbflash.retriveConfigDB(getApplicationContext(), DBFlash.USERCONF);
		User newUser = new User(uSave);
		dbSqlite = new DBBaseFacade(getApplicationContext(), newUser.getEmail());
		
		Intent intent = getIntent();
		String mod_id = intent.getStringExtra("mod_id");
		semantic = intent.getStringExtra("semantic");
		typeEnd = intent.getStringExtra("typeEnd");
		
		Module lModule = dbSqlite.retriveModuleById(Integer.valueOf(mod_id));
		ArrayList<Quiz> lQuiz = dbSqlite.retriveQuizByModule(lModule.getId(), 10);
		
		long seed = System.nanoTime();
		Collections.shuffle(lQuiz, new Random(seed));
		
		PlayModule playModule = new PlayModule(lModule);
		for (Quiz quiz : lQuiz) {
			ArrayList<QuizAlternative> lAlternative = dbSqlite.retriveAlternativeByQuiz(quiz.getId());
			seed = System.nanoTime();
			Collections.shuffle(lAlternative, new Random(seed));
			PlayQuiz playQuiz = new PlayQuiz(quiz, lAlternative);
			playModule.addQuiz(playQuiz);
		}
		
		play = pController.createPlayGame(playModule);
		
		adapter = new MyAlternativeArrayAdapter(getApplicationContext(), play.getModule().getQuizes().get(index).getAlternatives());
		lAlternative = (ListView) findViewById(R.id.listViewPlayGame);
		lAlternative.setAdapter(adapter);
		
		TextView emptyPView = (TextView) findViewById(R.id.quizEmptyP);
		lAlternative.setEmptyView(emptyPView);
		
		textQuestion = (TextView) findViewById(R.id.textViewPQuestion);
		textQuestion.setText(play.getModule().getQuizes().get(index).getQuiz().getQuestion());
		
		seq = (TextView) findViewById(R.id.textViewSequence);
		seq.setText(String.format("%s/%s", index+1, play.getModule().getQuizes().size()));
		
		btnNext = (Button) findViewById(R.id.btnQuizNext);
		btnNext.setEnabled(false);
		
		selected = new ArrayList<Integer>();
		for (int i = 0; i < play.getModule().getQuizes().size(); i++) {
			selected.add(i, -1);
		}
		
		lAlternative.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long id) {
				selected.add(index, position);
				btnNext.setEnabled(true);
				
			}
		});
		
		btnNext.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(index + 1 < play.getModule().getQuizes().size()) {
					btnNext.setEnabled(false);
					index += 1;
					changeView();
				} else {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PlayQuizActivity.this);
	                alertDialogBuilder.setTitle("Finish Quiz");
	                alertDialogBuilder
	                    .setMessage("Are your sure ?")
	                    .setCancelable(true);
	                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	                	

						@Override
						public void onClick(DialogInterface dialog, int which) {
							int result = calcResult(selected);
							if(typeEnd.equals("normal")) {
								Intent myIntent = new Intent(PlayQuizActivity.this,
										ResultQuizActivity.class);
								PlayQuizActivity.this.startActivity(myIntent);
								dbSqlite.insertIntoStatistic(play.getModule().getModule().getId(), result, play.getModule().getQuizes().size());
								String out = dbSqlite.retriveStatisticByModule(play.getModule().getModule().getId());
								dbflash.saveConfigDB(out, getApplicationContext(),
										DBFlash.QUIZRESULT);
								finish();
							}else { // extra
								if(semantic.equals("1")) {
									dbSqlite.deleteModule(play.getModule().getModule().getId());
								}
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PlayQuizActivity.this);
				                alertDialogBuilder.setTitle("Result Quiz");
				                alertDialogBuilder.setCancelable(false);
			                    if(result >= (play.getModule().getQuizes().size() / 2) + 1) {
			                    	alertDialogBuilder.setMessage("You win! Your score: " + result + " point(s)");
			                    }else {
			                    	alertDialogBuilder.setMessage("You lose! Your score: " + result + " point(s)");
			                    }
			                    alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										Intent myIntent = new Intent(PlayQuizActivity.this,
												ModuleTab.class);
										PlayQuizActivity.this.startActivity(myIntent);
										finish();
									}});
			                    alertDialogBuilder.show();
			                    
							}
						}
					});
	                AlertDialog alertDialog = alertDialogBuilder.create();
	                alertDialog.show();
				}
			}
		});
		
	}
	
	private void changeView() {
		textQuestion.setText(play.getModule().getQuizes().get(index).getQuiz().getQuestion());
		seq.setText(String.format("%s/%s", index+1, play.getModule().getQuizes().size()));
		adapter = new MyAlternativeArrayAdapter(getApplicationContext(), play.getModule().getQuizes().get(index).getAlternatives());
		lAlternative.setAdapter(adapter);
		
		int item = selected.get(index);
		if(item != -1) {
			lAlternative.setItemChecked(selected.get(index), true);
		}
		
	}
	
	private int calcResult(ArrayList<Integer> selected) {
		int total = 0;
		ArrayList<PlayQuiz> item = play.getModule().getQuizes();
		for ( int i= 0; i < item.size(); i++) {
			int chosen = selected.get(i);
			QuizAlternative alter = item.get(i).getAlternatives().get(chosen);
			if(alter.getCorrect() == 1) {
				total += 1;
			}
		}
		return total;
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.play_quiz, menu);
		return true;
	}
	
	private class MyAlternativeArrayAdapter extends ArrayAdapter<QuizAlternative>{
		private final Context context;
		private final ArrayList<QuizAlternative> values;

		public MyAlternativeArrayAdapter(Context context, ArrayList<QuizAlternative> values) {
			super(context, R.layout.play_quiz_list_item, values);
			this.context = context;
			this.values = values;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.play_quiz_list_item, parent,
					false);
			TextView tAlter = (TextView) rowView.findViewById(R.id.textViewPlAlternative);
			tAlter.setText(values.get(position).getAlternative());
			return rowView;
		}

	}

}
