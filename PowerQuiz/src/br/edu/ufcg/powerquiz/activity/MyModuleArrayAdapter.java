package br.edu.ufcg.powerquiz.activity;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.controller.UserController;
import br.edu.ufcg.powerquiz.model.Module;
import br.edu.ufcg.powerquiz.model.User;
import br.edu.ufcg.powerquiz.util.Util;

public class MyModuleArrayAdapter extends ArrayAdapter<Module> {
	private final Context context;
	private final ArrayList<Module> values;
	private UserController uController = UserController.getInstance();

	public MyModuleArrayAdapter(Context context, ArrayList<Module> values) {
		super(context, R.layout.module_item, values);
		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.module_item, parent,
				false);
		TextView textViewName = (TextView) rowView
				.findViewById(R.id.textViewModuleName);
		textViewName.setText(values.get(position).getName());
		TextView textViewAuthor = (TextView) rowView.findViewById(R.id.textViewAuthor);
		
		if(Util.isInteger(values.get(position).getOwn())) {
			User u = uController.getUserProfile(Integer.valueOf(values.get(position).getOwn()));
			textViewAuthor.setText(u.getName() + " " + u.getLastName());
		}else {
			textViewAuthor.setText(values.get(position).getOwn());
		}
		
		
		TextView textViewPrice = (TextView) rowView.findViewById(R.id.textViewPrice);
		int points = values.get(position).getPrice();
		if(points == 0) {
			textViewPrice.setText("FREE");
		}else {
			textViewPrice.setText(String.valueOf(points));
		}
		

		return rowView;
	}
}