package br.edu.ufcg.powerquiz.activity;

import java.util.ArrayList;

import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.controller.ModuleController;
import br.edu.ufcg.powerquiz.controller.UserController;
import br.edu.ufcg.powerquiz.db.DBFlash;
import br.edu.ufcg.powerquiz.model.Historical;
import br.edu.ufcg.powerquiz.model.User;
import br.edu.ufcg.powerquiz.util.DownloadImageTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class HistoricalActivity extends Activity {

	private ModuleController mController = ModuleController.getInstance();
	private UserController uController = UserController.getInstance();
	private ArrayList<Historical> lHistorical;
	private DBFlash dbflash = DBFlash.getInstace();
	User newUser;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_historical);

		String uSave = dbflash.retriveConfigDB(getApplicationContext(),
				DBFlash.USERCONF);
		newUser = new User(uSave);
		lHistorical = retriveHistoricalList(newUser.getId());
		ListView lvHistorical = (ListView) findViewById(R.id.listViewHistorical);
		final MyHistoricalArrayAdapter adapter = new MyHistoricalArrayAdapter(
				getApplicationContext(), lHistorical);
		lvHistorical.setAdapter(adapter);
		lvHistorical.setEmptyView(findViewById(R.id.textViewHistoricalNoItems));
		
		lvHistorical.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long id) {
				if(newUser.getId() == adapter.getItem(position).getUser1()) {
					AlertDialog.Builder ad = new AlertDialog.Builder(HistoricalActivity.this);
					ad.setTitle("Quiz Status");
					ad.setCancelable(true);
					if(adapter.getItem(position).getExecuted() == 1) {
						String winner = getChallengeWinner(adapter.getItem(position).getUser1(),adapter.getItem(position).getUser2(),adapter.getItem(position).getMod_id());
						ad.setMessage("Winner: " + winner);
					} else {
						ad.setMessage("Quiz not responded");
					}
 					
					ad.show();
				} else {
					// TODO Open playquiz. 
				}
				
			}

		});

	}

	protected String getChallengeWinner(int user1, int user2, int mod_id) {
		return mController.getChallengeWinner(user1, user2, mod_id);
	}

	private ArrayList<Historical> retriveHistoricalList(int user_id) {
		return mController.downloadChallenge(user_id);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.historical, menu);
		return true;
	}

	private class MyHistoricalArrayAdapter extends ArrayAdapter<Historical> {
		private final Context context;
		private final ArrayList<Historical> values;

		public MyHistoricalArrayAdapter(Context context,
				ArrayList<Historical> values) {
			super(context, R.layout.historical_item, values);
			this.context = context;
			this.values = values;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.historical_item, parent,
					false);

			ImageView iProfile = (ImageView) rowView
					.findViewById(R.id.imageViewHistoricalPhoto);
			ImageView arrow = (ImageView) rowView
					.findViewById(R.id.imageViewArrow);

			TextView name = (TextView) rowView
					.findViewById(R.id.textViewHistoricalName);
			TextView moduleName = (TextView) rowView
					.findViewById(R.id.textViewHistoricalMName);

			User u = null;
			if (newUser.getId() == values.get(position).getUser1()) {
				u = uController.getUserProfile(values.get(position).getUser2());
				if(values.get(position).getExecuted() == 0) {
					arrow.setImageDrawable(getResources().getDrawable(
						R.drawable.arrow_up));
				}else {
					arrow.setImageDrawable(getResources().getDrawable(
							R.drawable.arrow_up_gray));
				}
			} else {
				u = uController.getUserProfile(values.get(position).getUser1());
				if(values.get(position).getExecuted() == 0) {
					arrow.setImageDrawable(getResources().getDrawable(
						R.drawable.arrow_down));
				}else {
					arrow.setImageDrawable(getResources().getDrawable(
							R.drawable.arrow_down_gray));
				}
			}

			name.setText(u.getName() + " " + u.getLastName());
			moduleName.setText(mController.moduleDetails(values.get(position).getMod_id()).getName());

			DownloadImageTask dImage = new DownloadImageTask(iProfile);
			dImage.execute(u.getPicture());

			return rowView;
		}
	}

}
