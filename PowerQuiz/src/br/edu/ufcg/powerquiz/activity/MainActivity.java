package br.edu.ufcg.powerquiz.activity;

import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.controller.ModuleController;
import br.edu.ufcg.powerquiz.controller.UserController;
import br.edu.ufcg.powerquiz.db.DBFlash;
import br.edu.ufcg.powerquiz.model.User;
import br.edu.ufcg.powerquiz.util.DownloadImageTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	ModuleController mController = ModuleController.getInstance();
	UserController uController = UserController.getInstance();
	DBFlash dbFlash = DBFlash.getInstace();
	TextView points;
	User newUser;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		String uSave = dbFlash.retriveConfigDB(getApplicationContext(), DBFlash.USERCONF);
		newUser = new User(uSave);
		
		ImageView iProfile = (ImageView) findViewById(R.id.imageViewMainProfile);
		DownloadImageTask dImage = new DownloadImageTask(iProfile);
		dImage.execute(newUser.getPicture());
		
		Button btnCategory = (Button) findViewById(R.id.btnCallCategory);
		Button btnSearch = (Button) findViewById(R.id.btnModuleSearchActivity);
		Button btnModuleManager = (Button) findViewById(R.id.btnModuleManager);
		Button btnStartQuiz = (Button) findViewById(R.id.btnStarQuiz);
		Button btnSocialCenter = (Button) findViewById(R.id.btnSocialCenter);
		Button btnSearchUser = (Button) findViewById(R.id.btnSearchFriend);
		
		TextView userEmail = (TextView) findViewById(R.id.textViewUserEmail);
		TextView userName = (TextView) findViewById(R.id.textViewUserName);
		TextView userLastName = (TextView) findViewById(R.id.textViewUserLastName);
		points = (TextView) findViewById(R.id.textViewMainPoints);
		points.setText(uController.getPoints(newUser.getId()) + " Points");
		
		userEmail.setText(newUser.getEmail());
		userName.setText(newUser.getName());
		userLastName.setText(" " + newUser.getLastName());
		
		btnSearch.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(MainActivity.this, SearchModuleActivity.class);
            	MainActivity.this.startActivity(myIntent);
			}
		});
		
		btnCategory.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent myIntent = new Intent(MainActivity.this, CategoryActivity.class);
            	MainActivity.this.startActivity(myIntent);
            }
        });
		
		btnModuleManager.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(MainActivity.this, ModuleManagerActivity.class);
            	MainActivity.this.startActivity(myIntent);
				
			}
		});
		
		btnStartQuiz.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(MainActivity.this, ModuleTab.class);
            	MainActivity.this.startActivity(myIntent);
				
			}
		});
		
		btnSocialCenter.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(MainActivity.this, SocialCenterActivity.class);
            	MainActivity.this.startActivity(myIntent);
				
			}
		});
		
		btnSearchUser.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(MainActivity.this, ChallengeActivity.class);
            	MainActivity.this.startActivity(myIntent);
			}
		});
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	        finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int it = item.getItemId();
	    if( R.id.action_logout == it) {
	    	logoutApp();
	    }

		return true;
	}
	
	@Override
	protected void onResume() {
		points.setText(uController.getPoints(newUser.getId()) + " Points");
		super.onResume();
	}
	
	private void logoutApp() {
		dbFlash.saveConfigDB("", getApplicationContext(),
				DBFlash.USERCONF);
		dbFlash.saveConfigDB("", getApplicationContext(),
				DBFlash.USERREMEMBER);
		Intent myIntent = new Intent(MainActivity.this, LoginActivity.class);
    	MainActivity.this.startActivity(myIntent);
    	finish();
	}

}
