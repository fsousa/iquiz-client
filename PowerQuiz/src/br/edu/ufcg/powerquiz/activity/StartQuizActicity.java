package br.edu.ufcg.powerquiz.activity;

import java.util.ArrayList;

import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.db.DBBaseFacade;
import br.edu.ufcg.powerquiz.db.DBFlash;
import br.edu.ufcg.powerquiz.model.Module;
import br.edu.ufcg.powerquiz.model.User;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class StartQuizActicity extends Activity {
	
	private DBBaseFacade dbSqlite;
	private MyModuleArrayAdapter adapter;
	DBFlash dbflash = DBFlash.getInstace();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start_quiz_acticity);
		
		String uSave = dbflash.retriveConfigDB(getApplicationContext(), DBFlash.USERCONF);
		User newUser = new User(uSave);
		dbSqlite = new DBBaseFacade(getApplicationContext(), newUser.getEmail());
		
		ArrayList<Module> lModule = dbSqlite.retriveAllModule();
		ListView listViewModule = (ListView) findViewById(R.id.listViewModuleSelect);
		TextView emptyView = (TextView) findViewById(R.id.textViewDownloadModule);
		
		adapter = new MyModuleArrayAdapter(getApplicationContext(), lModule);
		listViewModule.setAdapter(adapter);
		listViewModule.setEmptyView(emptyView);
		
		listViewModule.setOnItemClickListener( new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long id) {
				Intent myIntent = new Intent(StartQuizActicity.this,
						PlayQuizActivity.class);
				myIntent.putExtra("mod_id", String.valueOf(adapter.getItem(position).getId()));
				myIntent.putExtra("semantic", "0");
				myIntent.putExtra("typeEnd", "normal");
				StartQuizActicity.this.startActivity(myIntent);
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.start_quiz_acticity, menu);
		return true;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	        finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}

}
