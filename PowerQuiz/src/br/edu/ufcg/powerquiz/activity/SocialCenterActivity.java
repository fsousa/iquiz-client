package br.edu.ufcg.powerquiz.activity;

import java.io.IOException;
import java.net.MalformedURLException;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
//import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;


import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.db.DBFlash;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class SocialCenterActivity extends Activity {

	private Facebook facebook;
	AsyncFacebookRunner mAsyncRunner;
	private TextView fcConn;
	private TextView TwConn;
	private Button btnFaceLogin;
	private Button btnTwitterLogin;
	

	// Twitter
	private static Twitter twitter; // Tutorial
									// http://www.androidhive.info/2012/09/android-twitter-oauth-connect-tutorial/
	private static RequestToken requestToken;

	// Twitter
	private DBFlash dbFlash = DBFlash.getInstace();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_social_center);

		String access_token = getFacebookToken();
		long expires = getFacebookExpire();

		fcConn = (TextView) findViewById(R.id.textViewFacebookConn);
		TwConn = (TextView) findViewById(R.id.textViewTwitterConn);
		facebook = new Facebook(DBFlash.FACEBOOKAPPID);

		btnFaceLogin = (Button) findViewById(R.id.btnConnectFacebook);
		btnTwitterLogin = (Button) findViewById(R.id.btnConnectTwitter);

		boolean fbStatus = checkFaceBookStatus(access_token, expires);
		if (fbStatus) {
			facebook.setAccessToken(access_token);
			facebook.setAccessExpires(expires);
			logoutFacebookBehavior();
		} else {
			loginFacebookBehavior();
		}
		
		if (!isTwitterLoggedInAlready()) {
			getLoginCredencial();
			loginTwitterBehavior();
		}else {
			logoutTwitterBehavior();
		}
		
	}

	private void getLoginCredencial() {
		Uri uri = getIntent().getData();
		if (uri != null
				&& uri.toString().startsWith(DBFlash.TWITTER_CALLBACK_URL)) {
			// oAuth verifier
			String verifier = uri
					.getQueryParameter(DBFlash.URL_TWITTER_OAUTH_VERIFIER);

			try {
				// Get the access token
				AccessToken accessToken = twitter.getOAuthAccessToken(
						requestToken, verifier);

				dbFlash.saveConfigDB(accessToken.getToken(),
						getApplicationContext(),
						DBFlash.PREF_KEY_OAUTH_TOKEN);
				dbFlash.saveConfigDB(accessToken.getTokenSecret(),
						getApplicationContext(),
						DBFlash.PREF_KEY_OAUTH_SECRET);
				dbFlash.saveConfigDB("true", getApplicationContext(),
						DBFlash.PREF_KEY_TWITTER_LOGIN);

				/*long userID = accessToken.getUserId();
				User user = twitter.showUser(userID);
				String username = user.getName();*/

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void logoutTwitterBehavior() {
		TwConn.setText("Configured");
		TwConn.setTextColor(Color.GREEN);
		logoutFromTwitter();
		btnTwitterLogin.setText("Remove Twitter");
		btnTwitterLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				logoutFromTwitter();
				loginTwitterBehavior();
				
			}
		});
		
	}

	private void loginTwitterBehavior() {
		TwConn.setText("Not Configured");
		TwConn.setTextColor(Color.RED);
		btnTwitterLogin.setText("Connect Twitter");
		btnTwitterLogin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				loginToTwitter();
			}
		});	
		
		
	}

	private void loginToTwitter() {
		// Check if already logged in
		if (!isTwitterLoggedInAlready()) {
			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(DBFlash.TWITTER_CONSUMER_KEY);
			builder.setOAuthConsumerSecret(DBFlash.TWITTER_CONSUMER_SECRET);
			Configuration configuration = builder.build();

			TwitterFactory factory = new TwitterFactory(configuration);
			twitter = factory.getInstance();

			try {
				requestToken = twitter
						.getOAuthRequestToken(DBFlash.TWITTER_CALLBACK_URL);
				this.startActivity(new Intent(Intent.ACTION_VIEW, Uri
						.parse(requestToken.getAuthenticationURL())));
			} catch (TwitterException e) {
				e.printStackTrace();
			}
			logoutTwitterBehavior();
		} else {
			// user already logged into twitter
			Toast.makeText(getApplicationContext(),
					"Already Logged into twitter", Toast.LENGTH_LONG).show();
		}
	}

	private boolean isTwitterLoggedInAlready() {
		String result = dbFlash.retriveConfigDB(getApplicationContext(),
				DBFlash.PREF_KEY_TWITTER_LOGIN);
		if (result.equals("true")) {
			return true;
		}
		return false;
	}
	
	private void logoutFromTwitter() {
		dbFlash.saveConfigDB("", getApplicationContext(),DBFlash.PREF_KEY_OAUTH_TOKEN);
		dbFlash.saveConfigDB("", getApplicationContext(),DBFlash.PREF_KEY_OAUTH_SECRET);
		dbFlash.saveConfigDB("", getApplicationContext(),DBFlash.PREF_KEY_TWITTER_LOGIN);
	
	}

	private Long getFacebookExpire() {
		String result = dbFlash.retriveConfigDB(getApplicationContext(),
				DBFlash.FACEBOOKEXPIRES);
		if (result == "") {
			return (long) 0;
		}
		return Long.valueOf(result);
	}

	private String getFacebookToken() {
		return dbFlash.retriveConfigDB(getApplicationContext(),
				DBFlash.FACEBOOKTOKEN);
	}

	private boolean checkFaceBookStatus(String access_token, long expires) {
		if (access_token != null && expires != 0) {
			return true;
		}
		return false;
	}

	private void logoutFacebookBehavior() {
		fcConn.setText("Configured");
		fcConn.setTextColor(Color.GREEN);
		btnFaceLogin.setText("Remove Facebook");

		btnFaceLogin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				logoutToFacebook();
				loginFacebookBehavior();
			}
		});
	}

	private void loginFacebookBehavior() {
		fcConn.setText("Not Configured");
		fcConn.setTextColor(Color.RED);
		btnFaceLogin.setText("Connect Facebook");
		btnFaceLogin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				loginToFacebook();
			}
		});
	}

	private void logoutToFacebook() {
		dbFlash.saveConfigDB("", getApplicationContext(), DBFlash.FACEBOOKTOKEN);
		dbFlash.saveConfigDB("0", getApplicationContext(),
				DBFlash.FACEBOOKEXPIRES);
		try {
			facebook.logout(getApplicationContext());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void loginToFacebook() {
		String access_token = getFacebookToken();
		long expires = getFacebookExpire();

		if (access_token != null) {
			facebook.setAccessToken(access_token);
		}

		if (expires != 0) {
			facebook.setAccessExpires(expires);
		}
		if (!facebook.isSessionValid()) {

			facebook.authorize(this,
					new String[] { "email", "publish_stream" },
					new DialogListener() {

						@Override
						public void onCancel() {
							// Function to handle cancel event
						}

						@Override
						public void onComplete(Bundle values) {
							// Function to handle complete event
							// Edit Preferences and update facebook acess_token

							dbFlash.saveConfigDB(facebook.getAccessToken(),
									getApplicationContext(),
									DBFlash.FACEBOOKTOKEN);
							dbFlash.saveConfigDB(
									String.valueOf(facebook.getAccessExpires()),
									getApplicationContext(),
									DBFlash.FACEBOOKEXPIRES);
							logoutFacebookBehavior();
						}

						@Override
						public void onFacebookError(FacebookError e) {
							e.printStackTrace();
						}

						@Override
						public void onError(DialogError e) {
							e.printStackTrace();
						}

					});
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.social_center, menu);
		return true;
	}

}
