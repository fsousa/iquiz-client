package br.edu.ufcg.powerquiz.activity;

import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.controller.UserController;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends Activity {
	
	UserController uController = UserController.getInstance();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		final EditText name = (EditText) findViewById(R.id.editTextNameRegister);
		final EditText lastName = (EditText) findViewById(R.id.editTextLastNRegister);
		final EditText email = (EditText) findViewById(R.id.editTextEmailRegister);
		final EditText pass = (EditText) findViewById(R.id.editTextPassRegister);
		final EditText again = (EditText) findViewById(R.id.editTextAgainRegister);
		
		
		Button btnSendRegister = (Button) findViewById(R.id.btnSendRegister);
		btnSendRegister.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				boolean validated = true;
				String sName = name.getText().toString();
				String sLastName = lastName.getText().toString();
				String sEmail = email.getText().toString();
				String sPass = pass.getText().toString();
				String sAgain = again.getText().toString();
				
				if( sName.length() < 3) {
					name.setError("Name must have at least 3 characters");
					validated = false;
				}
				else if( sLastName.length() < 3) {
					lastName.setError("Last Name must have at least 3 characters");
					validated = false;
				}
				else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(sEmail).matches()) {
					email.setError("Email is not valid");
					validated = false;
				}
				else if(!sPass.equals(sAgain)) {
					again.setError("Password dont match");
					validated = false;
				}
				else if(sPass.length() < 6) {
					pass.setError("Password must have at least 6 characters");
					validated = false;
				}
				
				if (validated) {
					SyncIncoData sync = new SyncIncoData();
					sync.execute(new String[] { name.getText().toString(),
							lastName.getText().toString(),
							email.getText().toString(),
							pass.getText().toString() });
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}

	private class SyncIncoData extends AsyncTask<String, String, String> {

		ProgressDialog dialog;

		@Override
		protected String doInBackground(String... params) {
			
			Boolean result = uController.registerUser(params[0], params[1], params[2], params[3]);
			if(result) {
				return "true";
			}
			return "false";
		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();

			dialog = ProgressDialog.show(RegisterActivity.this, "",
					"Loading. Please wait...", true);

		}

		@Override
		protected void onPostExecute(String response) {
			
			dialog.dismiss();
			if(response.equals("true")) {
				Toast.makeText(getApplicationContext(), "Register done.", Toast.LENGTH_LONG);
				Intent myIntent = new Intent(RegisterActivity.this,
						LoginActivity.class);
				RegisterActivity.this.startActivity(myIntent);
			}else {
				Toast.makeText(getApplicationContext(), "Something went wrong. Check your data", Toast.LENGTH_LONG).show();
			}

		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
		}

	}

}
