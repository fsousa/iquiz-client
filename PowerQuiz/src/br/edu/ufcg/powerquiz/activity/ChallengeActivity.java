package br.edu.ufcg.powerquiz.activity;

import br.edu.ufcg.powerquiz.R;
import android.os.Bundle;
import android.app.TabActivity;
import android.content.Intent;
import android.view.Menu;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

@SuppressWarnings("deprecation")
public class ChallengeActivity extends TabActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_challenge);
		
		TabHost tabHost = getTabHost(); 
	       
		TabSpec newQuizSpec = tabHost.newTabSpec("Find User");
		newQuizSpec.setIndicator("Find User", getResources().getDrawable(R.drawable.com_facebook_button_check));
        Intent NewQuizIntent = new Intent(this, SearchUserActivity.class);
        newQuizSpec.setContent(NewQuizIntent);
        
        TabSpec challengesSpec = tabHost.newTabSpec("Historical");
        challengesSpec.setIndicator("Historical", getResources().getDrawable(R.drawable.com_facebook_button_check));
        Intent photosIntent = new Intent(this, HistoricalActivity.class);
        challengesSpec.setContent(photosIntent);
        
        tabHost.addTab(newQuizSpec);
        tabHost.addTab(challengesSpec);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.challenge, menu);
		return true;
	}

}
