package br.edu.ufcg.powerquiz.activity;

import java.util.ArrayList;

import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.controller.ModuleController;
import br.edu.ufcg.powerquiz.controller.UserController;
import br.edu.ufcg.powerquiz.db.DBBaseFacade;
import br.edu.ufcg.powerquiz.db.DBFlash;
import br.edu.ufcg.powerquiz.model.Module;
import br.edu.ufcg.powerquiz.model.User;
import br.edu.ufcg.powerquiz.util.DownloadImageTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SearchUserActivity extends Activity {

	ArrayList<User> uList = null;
	UserController uController = UserController.getInstance();
	ModuleController mController = ModuleController.getInstance();
	int selectedItem = 0;
	ArrayList<Module> lModule;
	private DBBaseFacade dbSqlite;
	private DBFlash dbflash = DBFlash.getInstace();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_user);

		Button btnSearchUser = (Button) findViewById(R.id.btnSearchUserFriend);

		btnSearchUser.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				EditText uSearch = (EditText) findViewById(R.id.editTextSearchUser);
				uList = uController.searchUser(uSearch.getText().toString());

				MyUserArrayAdapter adapter = new MyUserArrayAdapter(
						getApplicationContext(), uList);
				ListView lUser = (ListView) findViewById(R.id.listViewSearchUser);
				lUser.setAdapter(adapter);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.search_user, menu);
		return true;
	}

	private class MyUserArrayAdapter extends ArrayAdapter<User> {
		private final Context context;
		private final ArrayList<User> values;

		public MyUserArrayAdapter(Context context, ArrayList<User> values) {
			super(context, R.layout.searchuser_item, values);
			this.context = context;
			this.values = values;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.searchuser_item, parent,
					false);
			ImageView iProfile = (ImageView) rowView
					.findViewById(R.id.imageViewUserImage);

			DownloadImageTask dImage = new DownloadImageTask(iProfile);
			dImage.execute(values.get(position).getPicture());

			TextView name = (TextView) rowView
					.findViewById(R.id.textViewUserName);
			TextView email = (TextView) rowView
					.findViewById(R.id.textViewUserEmail);
			Button challenge = (Button) rowView
					.findViewById(R.id.btnChallengeFriend);

			name.setText(values.get(position).getName() + " "
					+ values.get(position).getLastName());
			email.setText(values.get(position).getEmail());

			final int posi = position;
			challenge.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					
					String uSave = dbflash.retriveConfigDB(getApplicationContext(), DBFlash.USERCONF);
					final User newUser = new User(uSave);
					dbSqlite = new DBBaseFacade(getApplicationContext(), newUser.getEmail());
					lModule = dbSqlite.retriveAllModule();
					
					if(lModule.size() == 0) {
						Toast.makeText(getApplicationContext(), "You don't have modules.", Toast.LENGTH_LONG).show();
						return;
					}
					
					final String[] modules = alModuleToArray(lModule);
					AlertDialog.Builder ad = new AlertDialog.Builder(
							SearchUserActivity.this);
					ad.setTitle("Choose a module");
					ad.setSingleChoiceItems(modules, 0,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int item) {
									selectedItem = item;
								}
							});
					ad.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									int user1 = newUser.getId();
									int user2 = values.get(posi).getId();
									int mod_id = lModule.get(selectedItem).getId();
									boolean result = mController.addChallenge(user1, user2, mod_id);
									if(result) {
										Toast.makeText(getApplicationContext(), "Challenge sent.", Toast.LENGTH_LONG).show();
									}else {
										Toast.makeText(getApplicationContext(), "Something went wrong.", Toast.LENGTH_LONG).show();
									}
								}
							});
					ad.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {

								}
							});

					AlertDialog alert = ad.create();
					alert.show();

				}
			});

			return rowView;
		}
	}
	
	private String[] alModuleToArray(ArrayList<Module> alModule) {
		String[] result = new String[alModule.size()];
		for (int i = 0; i < alModule.size(); i++) {
			result[i] = alModule.get(i).getName();
		}
		return result;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

}
