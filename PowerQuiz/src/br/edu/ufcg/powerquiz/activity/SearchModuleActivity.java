package br.edu.ufcg.powerquiz.activity;

import java.util.ArrayList;

import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.controller.ModuleController;
import br.edu.ufcg.powerquiz.model.Module;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class SearchModuleActivity extends Activity {
	
	ModuleController mController = ModuleController.getInstance();
	ArrayList<Module> sResult = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_module);
		
		Button btnSearch = (Button) findViewById(R.id.btnSearch);
		
		btnSearch.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				EditText tSearch = (EditText) findViewById(R.id.editTextSearch);
				sResult = mController.searchModule(tSearch.getText().toString());
				MyModuleArrayAdapter adapter = new MyModuleArrayAdapter(getApplicationContext(), sResult);
				ListView lSearch = (ListView) findViewById(R.id.listViewSearch);
				lSearch.setAdapter(adapter);
				lSearch.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
						
						Intent myIntent = new Intent(SearchModuleActivity.this,
								ModuleDetailsActivity.class);
						myIntent.putExtra("moduleName", sResult.get(position).getName());
						myIntent.putExtra("des", sResult.get(position).getDescription());
						myIntent.putExtra("modId", Integer.toString(sResult.get(position).getId()));
						SearchModuleActivity.this.startActivity(myIntent);
					}
				});
				
			}
		});
		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	        finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.search_module, menu);
		return true;
	}
}
