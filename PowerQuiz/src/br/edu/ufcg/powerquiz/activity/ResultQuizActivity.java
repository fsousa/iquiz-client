package br.edu.ufcg.powerquiz.activity;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.db.DBFlash;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.BarGraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;

import com.facebook.android.Facebook;

public class ResultQuizActivity extends Activity {

	private DBFlash dbflash = DBFlash.getInstace();
	@SuppressWarnings("deprecation")
	private Facebook facebook = new Facebook(DBFlash.FACEBOOKAPPID);
	private DBFlash dbFlash = DBFlash.getInstace();
	int score;
	ProgressDialog pDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result_quiz);

		plotChart();

		Button btnShareFacebook = (Button) findViewById(R.id.btnShareFacebook);
		Button btnShareTwitter = (Button) findViewById(R.id.btnShareTwiiter);
		Button btnNewQuiz = (Button) findViewById(R.id.btnNewQuiz);
		
		boolean twitterStatus = twStatus();
		if(!twitterStatus) {
			btnShareTwitter.setEnabled(false);
		}
		btnShareTwitter.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				postToTwitter(score);
			}
		});
		
		
		boolean fbStatus = fbStatus();
		if(!fbStatus) {
			btnShareFacebook.setEnabled(false);
		}
		

		btnShareFacebook.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				postToWall(score);
			}
		});
		
		btnNewQuiz.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(ResultQuizActivity.this,
						ModuleTab.class);
				ResultQuizActivity.this.startActivity(myIntent);
				finish();
			}
		});
	}

	private boolean twStatus() {
		String result = dbFlash.retriveConfigDB(getApplicationContext(),
				DBFlash.PREF_KEY_TWITTER_LOGIN);
		if (result.equals("true")) {
			return true;
		}
		return false;
	}

	private void plotChart() {
		String result = dbflash.retriveConfigDB(getApplicationContext(),
				DBFlash.QUIZRESULT);
		String[] splited = result.split("#");
		int bigger = calcBiggerValue(splited);
		@SuppressWarnings("unused")
		int mean = calcMean(splited);
		score = Integer.valueOf(splited[splited.length - 1]
				.split(",")[1]);

		GraphViewData[] data = new GraphViewData[splited.length];
		
		for (int i = 0; i < splited.length; i++) {
			data[i]  = new GraphViewData(i, Integer.valueOf(splited[i]
					.split(",")[1]));
		}
		
		GraphViewSeries exampleSeries = new GraphViewSeries(data);
		

		GraphView graphView = new BarGraphView(
		      this // context
		      , "Quiz Result" // heading
		);
		  
		// optional - activate scaling / zooming 
		if(splited.length >= 13) {
			graphView.setScrollable(true);
			graphView.setViewPort(splited.length-12, 10);
			graphView.setScalable(true);
		}
		graphView.getGraphViewStyle().setNumVerticalLabels(bigger);
		graphView.getGraphViewStyle().setVerticalLabelsColor(Color.BLACK);  
		graphView.getGraphViewStyle().setHorizontalLabelsColor(Color.BLACK);  
		graphView.addSeries(exampleSeries); // data

		LinearLayout layout = (LinearLayout) findViewById(R.id.chartResult);
		layout.addView(graphView);
	}

	public void postToWall(int score) {
		FacebookAsyncData aSync = new FacebookAsyncData();
		aSync.execute(new String[] { String.valueOf(score) });

	}
	
	public void postToTwitter(int score) {
		updateTwitterStatus async = new updateTwitterStatus();
		async.execute(new String[] {String.valueOf(score) });
	}

	private Long getFacebookExpire() {
		String result = dbFlash.retriveConfigDB(getApplicationContext(),
				DBFlash.FACEBOOKEXPIRES);
		if (result == "") {
			return (long) 0;
		}
		return Long.valueOf(result);
	}

	private String getFacebookToken() {
		return dbFlash.retriveConfigDB(getApplicationContext(),
				DBFlash.FACEBOOKTOKEN);
	}
	
	private boolean fbStatus() {
		String token = getFacebookToken();
		long expires = getFacebookExpire();
		
		if(token != null && expires != 0 && token != "") {
			return true;
		}
		return false;
	}

	private int calcMean(String[] splited) {
		int mean = 0;
		for (int i = 0; i < splited.length; i++) {
			mean += Integer.valueOf(splited[i].split(",")[1]);
		}
		return mean / splited.length;
	}

	private int calcBiggerValue(String[] splited) {
		int bigger = Integer.valueOf(splited[0].split(",")[1]);
		for (int i = 1; i < splited.length; i++) {
			if (Integer.valueOf(splited[i].split(",")[1]) > bigger) {
				bigger = Integer.valueOf(splited[i].split(",")[1]);
			}
		}
		return bigger;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.result_quiz, menu);
		return true;
	}

	private class FacebookAsyncData extends AsyncTask<String, String, String> {

		ProgressDialog dialog;

		@SuppressWarnings("deprecation")
		@Override
		protected String doInBackground(String... params) {

			String access_token = getFacebookToken();
			long expires = getFacebookExpire();

			facebook.setAccessExpires(expires);
			facebook.setAccessToken(access_token);
			Bundle parameters = new Bundle();
			parameters.putString("message", "Acabei de fazer " + params[0]
					+ " ponto(s) no PowerQuiz");
			try {
				facebook.request("me/feed", parameters, "POST");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return "";
		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();

			dialog = ProgressDialog.show(ResultQuizActivity.this, "",
					"Sharing. Please wait...", true);

		}

		@Override
		protected void onPostExecute(String response) {

			dialog.dismiss();
			Toast.makeText(getApplicationContext(), "Posted", Toast.LENGTH_LONG)
					.show();

		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
		}

	}
	
	private class updateTwitterStatus extends AsyncTask<String, String, String> {
		 
	    /**
	     * Before starting background thread Show Progress Dialog
	     * */
	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        pDialog = new ProgressDialog(ResultQuizActivity.this);
	        pDialog.setMessage("Updating to twitter...");
	        pDialog.setIndeterminate(false);
	        pDialog.setCancelable(false);
	        pDialog.show();
	    }
	 
	    /**
	     * getting Places JSON
	     * */
	    protected String doInBackground(String... args) {
	        String status = args[0];
	        try {
	            ConfigurationBuilder builder = new ConfigurationBuilder();
	            builder.setOAuthConsumerKey(DBFlash.TWITTER_CONSUMER_KEY);
	            builder.setOAuthConsumerSecret(DBFlash.TWITTER_CONSUMER_SECRET);
	             
	            // Access Token 
	            String access_token = dbflash.retriveConfigDB(getApplicationContext(), DBFlash.PREF_KEY_OAUTH_TOKEN);
	            // Access Token Secret
	            String access_token_secret = dbflash.retriveConfigDB(getApplicationContext(),DBFlash.PREF_KEY_OAUTH_SECRET);
	             
	            AccessToken accessToken = new AccessToken(access_token, access_token_secret);
	            Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);
	             
	            // Update status
	            twitter.updateStatus("Acabei de fazer " + status + " ponto(s) no Power Quiz");
	             
	        } catch (TwitterException e) {
	            e.printStackTrace();
	        }
	        return "";
	    }
	 
	    /**
	     * After completing background task Dismiss the progress dialog and show
	     * the data in UI Always use runOnUiThread(new Runnable()) to update UI
	     * from background thread, otherwise you will get error
	     * **/
	    protected void onPostExecute(String file_url) {
	        // dismiss the dialog after getting all products
	        pDialog.dismiss();
	        // updating UI from Background Thread
	        runOnUiThread(new Runnable() {
	            @Override
	            public void run() {
	                Toast.makeText(getApplicationContext(),
	                        "Status tweeted successfully", Toast.LENGTH_SHORT)
	                        .show();
	            }
	        });
	    }
	 
	}

}
