package br.edu.ufcg.powerquiz.activity;

import java.util.ArrayList;

import br.edu.ufcg.powerquiz.R;
import br.edu.ufcg.powerquiz.controller.ModuleController;
import br.edu.ufcg.powerquiz.model.Module;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class ModuleListActivity extends Activity {
	
	ModuleController mController = ModuleController.getInstance();
	ArrayList<Module> lCategory = null;
	MyModuleArrayAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_module_list);

		ListView listViewCategory = (ListView) findViewById(R.id.listViewModule);

		Intent intent = getIntent();
		String catID = intent.getStringExtra("cat_id");
		lCategory = mController.listModuleByCategory(catID);

		adapter = new MyModuleArrayAdapter(
				getApplicationContext(), lCategory);
		
		View emptyView = findViewById(R.id.empty);
		listViewCategory.setEmptyView(emptyView);
		listViewCategory.setAdapter(adapter);
		

		listViewCategory.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				
				Intent myIntent = new Intent(ModuleListActivity.this,
						ModuleDetailsActivity.class);
				myIntent.putExtra("moduleName", lCategory.get(position).getName());
				myIntent.putExtra("des", lCategory.get(position).getDescription());
				myIntent.putExtra("modId", Integer.toString(lCategory.get(position).getId()));
				ModuleListActivity.this.startActivity(myIntent);
			}
		});
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
	    if ((keyCode == KeyEvent.KEYCODE_BACK))
	    {
	        finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.module_list, menu);
		return true;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		adapter.notifyDataSetChanged();
	}

}
